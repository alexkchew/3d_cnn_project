# 3d_cnn_project

The purpose of this project is to run convolutional neural networks (2D or 3D) on molecular dynamics (MD) simulations. This project is intended to speed up feature discovery by allowing the machine to learn properties of the system. 

This project was published in:
A. K. Chew, S. Jiang, W. Zhang, V. M. Zavala, and R. C. Van Lehn. "Fast predictions of liquid-phase acid-catalyzed reaction rates using molecular dynamics and convolutional neural networks." Chem. Sci. Science, 2020, 11, 12464-12476. [https://doi.org/10.1039/D0SC03261A]

Please see the Zenodo for more details about running the code:
https://zenodo.org/record/4460617#.YDA5XBNKh-V