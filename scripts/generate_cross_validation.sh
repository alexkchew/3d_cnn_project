#!/bin/bash
# generate_cross_validation.sh
# This script generates cross validation for deep CNN Networks.
#
# Written by: Alex K. Chew (05/15/2019)
#
# USAGE:
# bash generate_cross_validation.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

##################################
### CROSS VALIDATION VARIABLES ###
##################################

## DEFINING CROSS VALIDATION NAME LIST
declare -a cross_validation_name_list=("${1-solute}")  # "cosolvent"
# "cosolvent" "solute"
# "solute" "cosolvent"

## lOOPING
for cross_validation_name in ${cross_validation_name_list[@]}; do

## DEFINING DESIRED CROSS VALIDATION
# cross_validation_name="cosolvent" # "solute"  # solute cosolvent
cross_validation_type="leave_one_out"

## DEFINING DATABASE TYPE
#database_type="32_32"
# database_type="20_20_20_32ns_firstwith10"
# database_type="20_20_20_20ns_firstwith10"
# database_type="32_32_32_20ns_firstwith10"
# database_type="20_20_20_20ns_3channel_hydroxyl_firstwith10"
# database_type="20_20_20_20ns_solvent_only_firstwith10"
# database_type="20_20_20_20ns_4chan_hydroxyl_firstwith10"
# ----
database_type="${2-20_20_20_20ns_oxy_3chan}"
# "20_20_20_20ns_oxy_3chan"
# "20_20_20_20ns_firstwith10" # ALL ATOM EXAMPLE
# "32_32_32_20ns_oxy_3chan_firstwith10"
# "16_16_16_20ns_oxy_3chan_firstwith10"
# "32_32_32_20ns_oxy_3chan_firstwith10"
# "20_20_20_20ns_solvent_only_firstwith10"
# "20_20_20_20ns_3channel_hydroxyl_firstwith10"
# "20_20_20_20ns_firstwith10_oxy"
# "20_20_20_20ns_oxy_3chan"
# "16_16_16_20ns_oxy_3chan_firstwith10"
# "20_20_20_20ns_oxy_3chan"
# database_type="32_32_32_20ns_oxy_3chan_firstwith10"
# ----
# "32_32_32_20ns_3channel_hydroxyl_firstwith10" # "32_32_32_20ns_solvent_only_firstwith10"
# database_type="32_32_32_20ns_oxy_3chan_firstwith10"
# database_type="32_32_32_20ns_oxy_firstwith10"
# "16_16_16_20ns_oxy_firstwith10"
# "20_20_20_20ns_firstwith10_oxy"
# ---

# "20_20_20_20ns_oxy_3chan" <-- newest type
# "20_20_20_32ns_firstwith10"
# "20_20_20_20ns_firstwith10_oxy"
# "32_32_32_20ns_firstwith10"
# "32_32_32_32ns_first" # _withoxy
# 30_30_30_32ns_first
# 20_20_20_32ns_first
# database_type="20_20_20_40ns_first"
# "20_20_20_50ns_updated"
# 20_20_20_10ns_updated
# "20_20_20_10ns_allatomwithsoluteoxygen"
# 20_20_20_190ns
# 20_20_20_withdmso
# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"
# 20_20_20_rdf
# 20_20_20_10ns_allatomwithsoluteoxygen

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING NETWORK
#cnn_type="vgg16"
cnn_type="${3-solvent_net}"
# "vgg16"
# "solvent_net"
# "vgg16"
# "solvent_net"
# "orion"
# "solvent_net"
# "voxnet"
# "solvent_net"
# "solvent_net" # 
# orion
# voxnet
# solvent_net
# vgg16

## DEFINING CURRENT DIRECTORY
main_dir="${4-main_dir}"
# "20200501-cross_val_size-${database_type}-${cnn_type}-${cross_validation_name}" # _with_dmso

## DEFINING MAIN PATH
parent_dir="${5-${CNN_OUTPUT_DATA}}"

## DEFINING REPRESENTATION TYPE
representation_types="split_avg_nonorm"
# split_avg_nonorm_planar
# split_avg_nonorm
# "split_avg_nonorm_sampling_times"
# split_avg_nonorm_perc: split avg with a percentage off
#representation_types="split_avg_nonorm_planar"
# default: split_avg_nonorm
# split_avg_nonorm_50
# split_avg_nonorm_25
# split_avg_nonorm_01
# split_avg_nonorm_planar

## DEFINING IF YOU WANT JOBS TO BE SEPARATED
want_separate_jobs=false # true / false
## DEFINING DESCRIPTOR
want_descriptor=false # true false

## DEFINING IF YOU WANT A SUBMISSION SCRIPT WITH ALL CODES RUN IN PARALLEL
want_single_parallel_job=false
# true

## CHANGING REPRESENTATION FOR VGG16
if [[ "${cnn_type}" == "vgg16" ]]; then
    representation_types="split_avg_nonorm_planar"
fi

## DEFINING DETAILS OF SIMULATIONS
mass_frac="10,25,50,75" # 75
cosolvents="DIO,GVL,THF" # ,dmso,dmso dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,GVL,THF
solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"

## DEFINING SAMPLING TYPE
sampling_type="strlearn"
sampling_inputs=${6-"0.8"}

echo "Sampling inputs: ${sampling_inputs}"

## DEFINING NUMBER OF FOLDS
num_cross_valid_folds="${7-5}"

# "0.75" 
# 0.6 60% stratified learning
# 0.75 75% stratified learning

########################
### SAMPLING DETAILS ###
########################

## DEFINGING SPLITTING
split_chunks="10"
# "8" # "5"

## DEFINING NUMBER OF EPOCHS
epochs="500"

# 0.6 60% stratified learning

## DEFINING REPRESENTATION INPUTS
representation_inputs="${split_chunks}" # Number of splits 19

#####################
### DATABASE TYPE ###
#####################

## DEFINING OUTPUT SUBMISSION SCRIPT
output_bash_extraction_script="extract_deep_cnn.sh"

########################
### DEFINING OUTPUTS ###
########################

## DEFINING CROSS VALIDATION TEXT FILE NAME
cross_validation_file_name="cross_valid.txt"

## DEFINING OUTPUT BASH SCRIPT
output_bash_script="train_cross_valid.sh"

## DEFINING OUTPUT
output_sim_info="train_info.out"

## SUBMISSION SCRIPT
output_submit="submit.sh"

######################
### DEFINING PATHS ###
######################

## PATH TO SUBMISSION SCRIPT
path_submit_script="${CNN_SUBMIT_SCRIPTS}/submit.sh"

## DEFINING OUTPUT PATH
path_output="${parent_dir}/${main_dir}"

## CREATING OUTPUT DIRECTORY
create_dir "${path_output}" -f 

## DEFINING INPUT BASH SCRIPT
path_input_bash="${CNN_SCRIPT_DIR}/generate_deep_cnn.sh"

## DEFINING PATH TO OUTPUT ABSH
path_output_bash_run="${path_output}/${output_bash_script}"

## DEFINING OUTPUT PATH
path_output_submit_script="${path_output}/${output_submit}"

#############################################
### GENERATING CROSS VALIDATION TEXT FILE ###
#############################################

## DEFINING CROSS VALIDATION PYTHON SCRIPT
path_python_cross_valid="${CNN_PYTHON_CODES}/cross_validation.py"

## DEFINING PATH TO OUTPUT
path_output_cross_valid="${path_output}/${cross_validation_file_name}"

## RUNNING CROSS VALIDATION
python "${path_python_cross_valid}" \
                                    -s "${solutes}" \
                                    -x "${cosolvents}" \
                                    -m "${mass_frac}" \
						            -c "${cross_validation_name}" \
                                    -t "${cross_validation_type}" \
                                    -o "${path_output_cross_valid}" 

########################################
### READING CROSS VALIDATION DETAILS ###
########################################

## FINDING CROSS VALIDATION VARIABLE
cross_valid_variable="$(grep "CROSS_VALIDATION_NAME" "${path_output_cross_valid}" | awk '{print $2}')" # e.g. solute

## FINDING START AND END POINTS
start_line=$(grep -nE "CROSS_VALIDATION_START" "${path_output_cross_valid}"  | sed 's/\([0-9]*\).*/\1/')
end_line=$(grep -nE "CROSS_VALIDATION_END" "${path_output_cross_valid}"  | sed 's/\([0-9]*\).*/\1/')

## FOR A GIVEN VARIABLE, LOCATE ALL VARIATIONS AS AN ARRAY

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(sed -n "$((start_line+1)),$((end_line-1))p" "${path_output_cross_valid}" | grep -v -e '^;')"

## READING AS AN ARRAY
readarray -t cross_valid_variable_values <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"

## LOOPING OVER EACH VARIABLE
for each_variable in "${cross_valid_variable_values[@]}"; do
    ## DEFINING THE SOLUTES
    if [ "${cross_valid_variable}" == "solute" ]; then
        ## STORING THE SOLUTE
        solutes="${each_variable}"
    elif [ "${cross_valid_variable}" == "cosolvent" ]; then
    	cosolvents="${each_variable}"
    fi
    
    ## RUNNING BASH SCRIPT
    bash "${path_input_bash}" \
            "${main_dir}" \
            "${cnn_type}" \
            "${representation_types}" \
            "${representation_inputs}" \
            "${mass_frac}" \
            "${cosolvents}" \
            "${solutes}" \
            "${epochs}" \
            "${database_type}" \
            "${sampling_type}" \
            "${sampling_inputs}" \
            "${want_separate_jobs}" \
            "${want_descriptor}" \
            "${num_cross_valid_folds}" \
            "${want_single_parallel_job}" \
            "${parent_dir}"
            
    ## DEFINING OUTPUT NAME
    output_name="$(CNN_generate_name ${representation_types}\
                                     ${representation_inputs}\
                                     ${mass_frac} ${cosolvents} ${solutes} ${epochs} ${database_type} \
                                     ${cnn_type} \
                                    "${sampling_type}" \
                                    "${sampling_inputs}" \
                                    "${want_descriptor}" \
        )"
        
    
    ## GETTING THE OUTPUT NAME AS A BASH SCRIPT
    echo "####" >> "${path_output_bash_run}"
    echo "echo \"Check output at: ${path_output}/${output_sim_info}\"" >> "${path_output_bash_run}"
    echo "cd \"${path_output}/${output_name}\"; bash ${output_bash_extraction_script} > ${output_sim_info} &" >> "${path_output_bash_run}"
    #  2>&1

done

## WAITING FOR ALL FILES
echo "" >> "${path_output_bash_run}"
echo "wait" >> "${path_output_bash_run}"

## COPYING SUBMISSION SCRIPT
cp "${path_submit_script}" "${path_output_submit_script}"

## EDITING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${main_dir}/g" "${path_output_submit_script}"
sed -i "s/_BASHSCRIPT_/${output_bash_script}/g" "${path_output_submit_script}"

## ADDING TO JOB LIST
if [[ "${want_separate_jobs}" == false ]] && [[ "${want_single_parallel_job}" == false ]]; then
    echo "${path_output_submit_script}" >> "${CNN_JOB_LIST}"
fi


done