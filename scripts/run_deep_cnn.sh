#!/bin/bash
# run_deep_cnn.sh
# This script basically runs generate_deep_cnn.sh script
#
# Written by: Alex K. Chew (05/15/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

#####################
### DATABASE TYPE ###
#####################

## DEFINING DATABASE TYPE
#database_type="32_32"
# database_type="20_20_20_32ns_firstwith10" # _first_withoxy
# database_type="16_16_16_20ns_oxy_3chan_firstwith10"
database_type="${1-20_20_20_20ns_oxy_3chan_firstwith10}"
# "20_20_20_20ns_oxy_3chan"
# "32_32_32_20ns_oxy_3chan_firstwith10" # <-- 32 x 32 x 32
# "20_20_20_20ns_oxy_3chan" <-- main manuscript representation

# "20_20_20_200ns_oxy_3chan_firstwith10" # manuscript rep, with 200 ns
# "20_20_20_20ns_firstwith10" # allatom example
# "32_32_32_20ns_oxy_3chan_firstwith10"
# "20_20_20_20ns_oxy_3chan"
# "20_20_20_20ns_oxy_3chan"
# "32_32_32_20ns_oxy_3chan_firstwith10"
# "16_16_16_20ns_oxy_3chan_firstwith10"
# "20_20_20_20ns_firstwith10_oxy"
# "20_20_20_20ns_3channel_hydroxyl_firstwith10"
# "20_20_20_20ns_solvent_only_firstwith10" # Solvents only
# "20_20_20_20ns_oxy_3chan"
# "20_20_20_20ns_oxy_3chan"

# "20_20_20_20ns_oxy_3chan" <-- winning type
# "20_20_20_20ns_4chan_hydroxyl_firstwith10"
# "20_20_20_20ns_3channel_hydroxyl_firstwith10"
# database_type="20_20_20_20ns_solvent_only_firstwith10"
# database_type="32_32_32_20ns_oxy_3chan_firstwith10"
# 32_32_32_20ns_oxy_firstwith10
# 16_16_16_20ns_oxy_firstwith10
# "20_20_20_20ns_firstwith10_oxy"
# ---
# "20_20_20_20ns_oxy_3chan" <-- newest type
# "20_20_20_32ns_firstwith10"
# "20_20_20_32ns_firstwith10"
# 20_20_20_20ns_firstwith10_oxy
# "20_20_20_20ns_firstwith10"
# 32_32_32_20ns_firstwith10
# "32_32_32_20ns_firstwith10"
# "20_20_20_20ns_firstwith10_oxy"
# "20_20_20_20ns_firstwith10"
# 20_20_20_20ns_firstwith10_oxy
# "32_32_32_20ns"
# "16_16_16_20ns_firstwith10"
# "20_20_20_20ns_firstwith10"
# 20_20_20_32ns_first
# "30_30_30_32ns_first"
# 20_20_20_32ns_first
# 20_20_20_40ns_first
# 20_20_20_50ns_updated
# 20_20_20_10ns_updated
# "20_20_20_10ns_allatomwithsoluteoxygen"
# 20_20_20_190ns
# 20_20_20_withdmso
# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"
# 20_20_20_rdf
# 20_20_20_10ns_allatomwithsoluteoxygen

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING CURRENT DIRECTORY
# main_dir="190807-individual_solvents_${database_type}" # GVL_THF_
main_dir="${2-20200505-Rerun_training_new_augmentation_200ns}"
# "20200429-200ns_training_test"
# "20200302-32x32_training"
# ${database_type}
# "2020203-5fold_train_${database_type}" # GVL_THF_

## DEFINING NETWORK
#cnn_type="vgg16"
declare -a cnn_types=("$3")
#  "voxnet" "orion"
# "voxnet" "orion"
# "orion" "voxnet"
# "solvent_net"
# "vgg16"
# solvent_net
# "vgg16") #  "orion"  "voxnet" "orion" 
# "orion" "voxnet"
# "solvent_net"
# "solvent_net"
# "voxnet" "solvent_net" "orion"
# "voxnet" "solvent_net" 
# "voxnet" "solvent_net" "orion"  ""
# "vgg16"

## DEFINING DETAILS OF SIMULATIONS
mass_frac="10,25,50,75" # 75
cosolvents="${4-DIO,GVL,THF}"

## DEFINING PARENT DIR
parent_dir="${5-${CNN_OUTPUT_DATA}}"

# "DIO,GVL,THF"
# ,GVL,THF
# "THF"
# "DIO,GVL,THF" # ,GVL,THF
# DIO,GVL,THF
# ,GVL,THF DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"

########################
### SAMPLING DETAILS ###
########################


## DEFINING DESCRIPTOR
want_descriptor=false

## ALL OTHER
## DEFINGING SPLITTING
if [[ "${database_type}" == "20_20_20_200ns_oxy_3chan_firstwith10" ]]; then
    echo "Switching splitting chunks to 100 for 200 ns"
    split_chunks="100"
else
    echo "Splitting chunks: 10"
    split_chunks="10" # "8" # "5"
fi
sleep 2

## DEFINING SAMPLING TYPE
sampling_type="strlearn"
sampling_inputs="${6-0.80}"
# "0.80" # "0.75" 
# 0.6 60% stratified learning
## DEFINING NUMBER OF FOLDS
num_cross_valid_folds="${7-5}"


## DEFINING NUMBER OF EPOCHS
epochs="500"

# DEFAULT
representation_inputs="${split_chunks}" # Number of splits 19

## DEFINING INPUT BASH SCRIPT
path_input_bash="${CNN_SCRIPT_DIR}/generate_deep_cnn.sh"

## LOOPING THROUGH EACH CNN TYPE
for cnn_type in ${cnn_types[@]}; do

    ## CHANGING REPRESENTATION FOR VGG16
    if [[ "${cnn_type}" == "vgg16" ]]; then
        representation_types="split_avg_nonorm_planar"
        if [[ "${database_type}" != "32_32_32_20ns_oxy_3chan_firstwith10" ]]; then 
            echo "Warning, vgg16 selected but the database type is not 32_32_32_20ns_oxy_3chan_firstwith10"
            echo "If your data type is 32 x 32, then continue. Otherwise, check your data type!"
            echo "Current database type: ${database_type}"
            echo "Sleeping for 3 seconds ..."
            sleep 3
        fi
    else
        representation_types="split_avg_nonorm"
    fi
    
    ## CHECKING REPRESENTATION TYPE
    if [ "${representation_types}" != "split_avg_nonorm" ]; then
        echo "Representation type is not split_avg_norm!"
        echo "Current representation type: ${representation_types}"
        echo "Make sure that is correct!"
        sleep 2
    fi

    ## RUNNING BASH SCRIPT
    bash "${path_input_bash}" \
            "${main_dir}" \
            "${cnn_type}" \
            "${representation_types}" \
            "${representation_inputs}" \
            "${mass_frac}" \
            "${cosolvents}" \
            "${solutes}" \
            "${epochs}" \
            "${database_type}" \
            "${sampling_type}" \
            "${sampling_inputs}" \
            "true" \
            "${want_descriptor}" \
            "${num_cross_valid_folds}" \
            "true" \
            "${parent_dir}"

done
