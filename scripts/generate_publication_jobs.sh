#!/bin/bash
# generate_publication_jobs.sh
# The purpose of this script is to basically go through the entire manuscript 
# and generate jobs for each part. By running this, you will generate 
# all the jobs necessary to publish the manuscript (assuming you have 
# the resouces to run it!)
#
# Written by Alex K. Chew (05/05/2020)
#
# USAGE:
#	bash generate_publication_jobs.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"


## DEFINING RUN SCRIPT
run_script="run_deep_cnn.sh"
cross_valid_script="generate_cross_validation.sh"

## SI SCRIPTS
si_varying_train_size="sampling_time_increments_varying_training_size.sh"
si_vary_time_chunks="sampling_time_chunks.sh"

## DEFINING GPU FOLDER
gpu_folder="${CNN_OUTPUT_DATA}/GPU_FOLDER"

##################
### MANUSCRIPT ###
##################

## COMMENT ANY OF THESE OUT IF YOU DO NOT WANT TO RUN THESE JOBS

## DECLARING TYPES
declare -a run_these_jobs=(\
	# "MANUSCRIPT_0_TRAINING_3DCNNS_ALLDATA" \
	# "MANUSCRIPT_1_CROSSVALID_ALLDATA" \
	# "MANUSCRIPT_0_TRAINING_5FOLD" \
	# DEPRECIATED --  "MANUSCRIPT_1_CROSSVALID" \
	"SI_0A_Sampling_across_training_size" \
	# "SI_0B_Sampling_vs_time_chunks" \
	# "SI_1_TRAINING_EACH_SOLVENT" \
	# "SI_2A_Training_different_voxel_inputs" \
	# DEPRECIATED -- "SI_2B_Cross_validating" \
	# "SI_3A_Training_32_32_32" \
	# DEPRECIATED -- "SI_3B_Cross_validating_32_32_32" \
	# "TEST_EXAMPLE_3" \
	#  "TEST_100_SAMPLING" \
	# "SI_0D_200NS_SAMPLING"
	)

## DEFINING MAIN DATABAES TYPE
MAIN_DATABASE_TYPE="20_20_20_20ns_oxy_3chan"
SAMPLING_INPUTS="1.00"
NUM_CROSS_VALID_FOLDS="5"

##########################################
### TRAINING VOXNET, SOLVENTNET, ORION ###
##########################################
if [[ " ${run_these_jobs[@]} " =~ " MANUSCRIPT_0_TRAINING_5FOLD " ]]; then

	declare -a cnn_types=("voxnet" "solvent_net" "orion")
	# "voxnet" "solvent_net" "orion"
	## DEFINING MAIN DIR
	main_dir="MANUSCRIPT_0_TRAINING_5FOLD"
	database_type="${MAIN_DATABASE_TYPE}"
	# "20_20_20_200ns_oxy_3chan"
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## DEFINING NUM CROSS VALID FOLDS
	num_cross_valid_folds="${NUM_CROSS_VALID_FOLDS}"

	# RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then

	for cnn_type in ${cnn_types[@]}; do
		bash "${run_script}" \
					"${database_type}" \
					"${main_dir}" \
					"${cnn_type}" \
					"${cosolvents}" \
					"${CNN_OUTPUT_DATA}" \
					"${sampling_inputs}" \
					"${num_cross_valid_folds}"
	done	

	else
		echo "${main_dir} exists! Continuing..."
	fi

fi

#####################################################
### TRAINING WITH NO CROSS VALIDATION -- ALL DATA ###
#####################################################
if [[ " ${run_these_jobs[@]} " =~ " MANUSCRIPT_0_TRAINING_3DCNNS_ALLDATA " ]]; then
	## DECLARING CNN
	declare -a cnn_types=("voxnet" "solvent_net" "orion")
	main_dir="MANUSCRIPT_0_TRAINING_3DCNNS_ALLDATA"
	database_type="${MAIN_DATABASE_TYPE}"
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## DIVIDING NUM CROSS VALIDATION FOLDS
	num_cross_valid_folds="0"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then

		for cnn_type in ${cnn_types[@]}; do
			bash "${run_script}" \
						"${database_type}" \
						"${main_dir}" \
						"${cnn_type}" \
						"${cosolvents}" \
						"${CNN_OUTPUT_DATA}" \
						"${sampling_inputs}" \
						"${num_cross_valid_folds}"
		done

	else
		echo "${main_dir} exists! Continuing..."
	fi



fi

###################################
### CROSS VALIDATING (ALL DATA) ###
###################################

if [[ " ${run_these_jobs[@]} " =~ " MANUSCRIPT_1_CROSSVALID_ALLDATA " ]]; then

	declare -a cross_validation_name_list=("solute")
	#  "cosolvent"
	# ("cosolvent" "solute")  # "cosolvent"
	declare -a cnn_types=("voxnet")
	#  "solvent_net" "orion"
	# ("voxnet")
	# ("voxnet" "solvent_net" "orion")

	## DEFINING PREFIX
	main_dir_prefix="MANUSCRIPT_1_CROSSVALID_ALLDATA"
	database_type="${MAIN_DATABASE_TYPE}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## DEFINING NUMBER OF CROSS VALIDS
	num_cross_valid_folds="0"

	## LOOPING THROUGH EACH
	for cnn_type in ${cnn_types[@]}; do
		for cross_valid_name in ${cross_validation_name_list[@]}; do		
			## DEFINING MAIN DIR
			main_dir="${main_dir_prefix}_${database_type}_${cnn_type}_${cross_valid_name}"

			## DEFINING DIRECTORY
			path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

			if [[ ! -e "${path_to_main_dir}" ]]; then
				## RUNNING
				bash "${cross_valid_script}" \
						"${cross_valid_name}" \
						"${database_type}" \
						"${cnn_type}" \
						"${main_dir}" \
						"${CNN_OUTPUT_DATA}" \
						"${sampling_inputs}" \
						"${num_cross_valid_folds}"
				else
					echo "${main_dir} exists! Continuing..."
				fi

		done
	done

fi



########################
### CROSS VALIDATING ###
########################

if [[ " ${run_these_jobs[@]} " =~ " MANUSCRIPT_1_CROSSVALID " ]]; then

	declare -a cross_validation_name_list=("solute")
	# ("cosolvent" "solute")  # "cosolvent"
	declare -a cnn_types=("voxnet")
	# ("voxnet" "solvent_net" "orion")

	## DEFINING PREFIX
	main_dir_prefix="MANUSCRIPT_1_CROSSVALID_100_sampling"
	database_type="${MAIN_DATABASE_TYPE}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## LOOPING THROUGH EACH
	for cnn_type in ${cnn_types[@]}; do
		for cross_valid_name in ${cross_validation_name_list[@]}; do		
			## DEFINING MAIN DIR
			main_dir="${main_dir_prefix}_${database_type}_${cnn_type}_${cross_valid_name}"

			## DEFINING DIRECTORY
			path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

			if [[ ! -e "${path_to_main_dir}" ]]; then
				## RUNNING
				bash "${cross_valid_script}" \
						"${cross_valid_name}" \
						"${database_type}" \
						"${cnn_type}" \
						"${main_dir}" \
						"${CNN_OUTPUT_DATA}" \
						"${sampling_inputs}"
				else
					echo "${main_dir} exists! Continuing..."
				fi

		done
	done

fi
#############################
### SUPPORING INFORMATION ###
#############################

######################################################
### S0A: Sampling time with varying training sizes ###
######################################################
if [[ " ${run_these_jobs[@]} " =~ " SI_0A_Sampling_across_training_size " ]]; then
	main_dir="SI_0A_Sampling_across_training_size_rerun"
	# SI_0A_Sampling_across_training_size
	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## RUNNING SCRIPT
	if [[ ! -e "${path_to_main_dir}" ]]; then
		bash "${si_varying_train_size}" "${main_dir}"
	else
		echo "${main_dir} exists! Continuing..."
	fi

fi

#############################################
### S0B: Sampling time versus time chunks ###
#############################################
if [[ " ${run_these_jobs[@]} " =~ " SI_0B_Sampling_vs_time_chunks " ]]; then
	main_dir="SI_0B_Sampling_vs_time_chunks"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## RUNNING SCRIPT
	if [[ ! -e "${path_to_main_dir}" ]]; then
		bash "${si_vary_time_chunks}" "${main_dir}"
	else
		echo "${main_dir} exists! Continuing..."
	fi
fi

######################################
### S1: TRAINING WITH EACH SOLVENT ###
######################################
if [[ " ${run_these_jobs[@]} " =~ " SI_1_TRAINING_EACH_SOLVENT " ]]; then
	main_dir="SI_1_TRAINING_EACH_SOLVENT"

	declare -a cnn_types=("voxnet" "solvent_net" "orion")
	## DEFINING MAIN DIR
	main_dir="SI_1_TRAINING_EACH_SOLVENT"
	database_type="${MAIN_DATABASE_TYPE}"
	declare -a cosolvents_list=("DIO" "GVL" "THF")

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then
		## LOOPING THROUGH CNN TYPES
		for cnn_type in ${cnn_types[@]}; do
			## LOOPING THROUGH COSOLVENTS
			for cosolvents in ${cosolvents_list[@]}; do
				bash "${run_script}" \
							"${database_type}" \
							"${main_dir}" \
							"${cnn_type}" \
							"${cosolvents}" \
							"${CNN_OUTPUT_DATA}" \
							"${sampling_inputs}"
			done
		done

	else
		echo "${main_dir} exists! Continuing..."
	fi

fi

####################################
### S2A: TRAINING DIFFERENT CNNS ###
####################################
if [[ " ${run_these_jobs[@]} " =~ " SI_2A_Training_different_voxel_inputs " ]]; then

	main_dir="SI_2A_Training_different_voxel_inputs"

	## VARYING DATABASE TYPE
	declare -a database_type_list=("20_20_20_20ns_3channel_hydroxyl_firstwith10" \
								   "20_20_20_20ns_solvent_only_firstwith10" \
								   "20_20_20_20ns_firstwith10" \
								   "20_20_20_20ns_firstwith10_oxy" \
								   "16_16_16_20ns_oxy_3chan_firstwith10"
									)

	## FOR CNN TYPES
	declare -a cnn_types=("solvent_net")

	## DEFINING COSOLVENTS
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then
		## LOOPING THROUGH CNN TYPES
		for cnn_type in ${cnn_types[@]}; do
			## LOOPING THROUGH COSOLVENTS
			for database_type in ${database_type_list[@]}; do
				## RUNNING SCRIPT
				bash "${run_script}" \
										"${database_type}" \
										"${main_dir}" \
										"${cnn_type}" \
										"${cosolvents}" \
										"${CNN_OUTPUT_DATA}" \
										"${sampling_inputs}"
			done
		done


	fi

fi

###############################################################
### S2: CROSS VALIDATING WITH THE DIFFERENT REPRESENTATIONS ###
###############################################################

if [[ " ${run_these_jobs[@]} " =~ " SI_2B_Cross_validating " ]]; then

	declare -a cross_validation_name_list=("cosolvent" "solute")  # "cosolvent"
	declare -a cnn_types=("solvent_net")

	## DEFINING PREFIX
	main_dir_prefix="SI_2B_Cross_validating"

	## VARYING DATABASE TYPE
	declare -a database_type_list=("20_20_20_20ns_3channel_hydroxyl_firstwith10" \
								   "20_20_20_20ns_solvent_only_firstwith10" \
								   "20_20_20_20ns_firstwith10" \
								   "20_20_20_20ns_firstwith10_oxy" \
								   "16_16_16_20ns_oxy_3chan_firstwith10"
									)

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## LOOPING THROUGH EACH DATABASE
	for database_type in ${database_type_list[@]}; do
		for cnn_type in ${cnn_types[@]}; do
			for cross_valid_name in ${cross_validation_name_list[@]}; do		
				## DEFINING MAIN DIR
				main_dir="${main_dir_prefix}_${database_type}_${cnn_type}_${cross_valid_name}"

				## DEFINING DIRECTORY
				path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

				if [[ ! -e "${path_to_main_dir}" ]]; then
					## RUNNING
					bash "${cross_valid_script}" \
							"${cross_valid_name}" \
							"${database_type}" \
							"${cnn_type}" \
							"${main_dir}" \
							"${CNN_OUTPUT_DATA}" \
							"${sampling_inputs}"
					else
						echo "${main_dir} exists! Continuing..."
				fi

			done
		done

	done

fi

###############################
### S3A: 32 X 32 X 32 SIZES ###
###############################

if [[ " ${run_these_jobs[@]} " =~ " SI_3A_Training_32_32_32 " ]]; then

	## TRAINING
	main_dir="SI_3A_Training_32_32_32"

	## VARYING DATABASE TYPE
	declare -a database_type_list=("32_32_32_20ns_oxy_3chan_firstwith10")

	## FOR CNN TYPES
	declare -a cnn_types=("solvent_net" "vgg16")

	## DEFINING COSOLVENTS
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${gpu_folder}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then
		## LOOPING THROUGH CNN TYPES
		for cnn_type in ${cnn_types[@]}; do
			## LOOPING THROUGH COSOLVENTS
			for database_type in ${database_type_list[@]}; do

				bash "${run_script}" \
										"${database_type}" \
										"${main_dir}" \
										"${cnn_type}" \
										"${cosolvents}" \
										"${gpu_folder}" \
										"${sampling_inputs}"
				done
		done


	fi

fi

#############################
### S3B: CROSS VALIDATING ###
#############################

if [[ " ${run_these_jobs[@]} " =~ " SI_3B_Cross_validating_32_32_32 " ]]; then

	declare -a cross_validation_name_list=("cosolvent" "solute")  # "cosolvent"
	declare -a cnn_types=("solvent_net" "vgg16")

	## DEFINING PREFIX
	main_dir_prefix="SI_3B_Cross_validating_32_32_32"

	## VARYING DATABASE TYPE
	declare -a database_type_list=("32_32_32_20ns_oxy_3chan_firstwith10")

	## DEFINING SAMPLING INPUTS
	sampling_inputs="${SAMPLING_INPUTS}"

	## LOOPING THROUGH EACH DATABASE
	for database_type in ${database_type_list[@]}; do
		for cnn_type in ${cnn_types[@]}; do
			for cross_valid_name in ${cross_validation_name_list[@]}; do		
				## DEFINING MAIN DIR
				main_dir="${main_dir_prefix}_${database_type}_${cnn_type}_${cross_valid_name}"

				## DEFINING DIRECTORY
				path_to_main_dir="${gpu_folder}/${main_dir}"

				if [[ ! -e "${path_to_main_dir}" ]]; then
					## RUNNING
					bash "${cross_valid_script}" \
							"${cross_valid_name}" \
							"${database_type}" \
							"${cnn_type}" \
							"${main_dir}" \
							"${gpu_folder}" \
							"${sampling_inputs}"
					else
						echo "${main_dir} exists! Continuing..."
					fi

			done
		done

	done

fi

#########################
### TESTING EXAMPLE 3 ###
#########################
# For this test, generate training /test sets available at: generate_separate_train_test_instances.py

if [[ " ${run_these_jobs[@]} " =~ " TEST_EXAMPLE_3 " ]]; then

	declare -a cnn_types=("solvent_net")
	## DEFINING MAIN DIR
	main_dir="TEST_EXAMPLE_3"
	database_type="20_20_20_20ns_oxy_3chan_train_0_5"
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then

		for cnn_type in ${cnn_types[@]}; do
			bash "${run_script}" \
						"${database_type}" \
						"${main_dir}" \
						"${cnn_type}" \
						"${cosolvents}"
		done

	else
		echo "${main_dir} exists! Continuing..."
	fi
fi


### TESTING FOR 100% SAMPLING

if [[ " ${run_these_jobs[@]} " =~ " TEST_100_SAMPLING " ]]; then

	declare -a cnn_types=("orion" "voxnet")
	# "solvent_net"
	## DEFINING MAIN DIR
	main_dir="TEST_100_SAMPLING_others"
	database_type="20_20_20_20ns_oxy_3chan"
	cosolvents="DIO,GVL,THF"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="1.00"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then
		## LOOPING THROUGH
		for cnn_type in ${cnn_types[@]}; do
			bash "${run_script}" \
						"${database_type}" \
						"${main_dir}" \
						"${cnn_type}" \
						"${cosolvents}" \
						"${CNN_OUTPUT_DATA}" \
						"${sampling_inputs}"
		done

	else
		echo "${main_dir} exists! Continuing..."
	fi

fi



##########################################
### TRAINING VOXNET, SOLVENTNET, ORION ###
##########################################
if [[ " ${run_these_jobs[@]} " =~ " SI_0D_200NS_SAMPLING " ]]; then

	declare -a cnn_types=("solvent_net")
	# "voxnet" "solvent_net" "orion"
	## DEFINING MAIN DIR
	main_dir="SI_0D_200NS_SAMPLING"
	database_type="20_20_20_200ns_oxy_3chan_firstwith10"
	# "20_20_20_200ns_oxy_3chan"
	cosolvents="DIO,GVL,THF"

	## DEFINING DIRECTORY
	path_to_main_dir="${CNN_OUTPUT_DATA}/${main_dir}"

	## DEFINING SAMPLING INPUTS
	sampling_inputs="1.00"

	## RUNNING
	if [[ ! -e "${path_to_main_dir}" ]]; then

		for cnn_type in ${cnn_types[@]}; do
			bash "${run_script}" \
						"${database_type}" \
						"${main_dir}" \
						"${cnn_type}" \
						"${cosolvents}" \
						"${CNN_OUTPUT_DATA}" \
						"${sampling_inputs}"
		done

	else
		echo "${main_dir} exists! Continuing..."
	fi

fi


