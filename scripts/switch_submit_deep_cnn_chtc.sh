#!/bin/bash
# submit_switch_deep_cnn_chtc.sh
# The purpose of this script is to switch all deep_cnn code to chtc
# INPUTS:
#   $1: simulation name
#   $2: path to multiple simulations
#   $3: gpu enabled? true or false
# This script will output to a job text file
# You simply just submit the job with the job_submit function
# if you want multiple sims:  
#   bash switch_submit_deep_cnn_chtc.sh MULTIPLE /home/akchew/scratch/3d_cnn_project/simulations/20200227-jobs false
#   For GPUs:
#   bash switch_submit_deep_cnn_chtc.sh "20200227-cross_val_size-32_32_32_20ns_oxy_3chan_firstwith10-solvent_net-cosolvent" "" true
#   FOR CPUTS
#   bash switch_submit_deep_cnn_chtc.sh 20200505-Rerun_training_new_augmentation "" "false"
# FOR HIGH MEMORY
#   bash switch_submit_deep_cnn_chtc.sh TEST_200NS_100_SAMPLING "" "true" "true"
#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## DEFINING CHTC RC
chtc_rc="chtc_rc.sh"

## DEFINING SIMULATION FOLDER
sim_folder="$1" # MULTIPLE FLAG

## DEFINING SPECIFIC DIR
path_to_multiple_sim="${2-""}"

## DEFINING IF YOU WANT GPUS
want_gpu="${3-true}"

## DEFINING IF YOU WANT LARGE MEMORY JOBS
want_large_memory="${4-false}"

## DEFINING GPU LENGTH
gpu_length="short"
# "short" - 12 hours
# "medium" - 24 hours
# "long" - 7 days

## SUBMISSION BASED ON GPU
if [[ "${want_gpu}" != true ]]; then
    run_script="run.sh"
    submit_script="submit.sub"
    request_cpu="1"
    ## DEFINING USAGE REQUIREMENTS
    request_disk="5GB"
    # "10GB"
    request_memory="20GB"
    # "20GB"

else
    ## GPU ENABLED
    run_script="run_gpu.sh"
    submit_script="submit_gpu.sub"
    request_cpu="1"
    ## DEFINING USAGE REQUIREMENTS
    request_disk="5GB"
    # "15GB"
    request_memory="20GB"
    # "35GB"
fi

## DEFINING HIGH MEMORY JOBS
if [[ "${want_large_memory}" == true ]]; then
    
    ## DEFINING USAGE REQUIREMENTS
    request_disk="10GB"
    ## GPU LENGTH
    gpu_length="short"
    # "medium"
    # "15GB"
    request_memory="100GB"
    echo "High memory job requested!"
    echo "Disk space: ${request_disk} // RAM space: ${request_memory}"
fi

## DEFINING PATHS
path_chtc_scripts="${main_dir}/chtc_scripts"
path_chtc_rc="${path_chtc_scripts}/chtc_rc.sh"
path_chtc_sub="${path_chtc_scripts}/${submit_script}"
path_chtc_run="${path_chtc_scripts}/${run_script}"

## DEFINING OUTPUT SUBMISSION
output_sub="submit.sub"

### DEFAULT INPUT FILES
instance_text_file="instance_name.txt"
initialize_summary="indices.summary"

## DEFINING JOB LIST
job_list="${CNN_JOB_LIST}"

## IF SIM FOLDER IS MULTIPLE, THEN IT WILL LOOP THROUGH ALL TRAJECTORIES
if [[ "${sim_folder}" == "MULTIPLE" ]]; then
    echo "Since MULTIPLE is selected, then we will loop through all potential path sims"
    
    ## PRINTING ERROR
    if [ -z "${path_to_multiple_sim}" ]; then
        echo "Error! Multiple flag is set on but no path has been included"
        exit 1
    fi
    
    ## GETTING DIRECTORIES
    read -a path_multiple_dir <<< $(ls ${path_to_multiple_sim}/*/ -d)

else
    ## DEFINING PATH TO SIM
    default_path_to_sim="${CNN_OUTPUT_DATA}/${sim_folder}"
    ## DEFINING PATHS
    declare -a path_multiple_dir=("${default_path_to_sim}")

fi

## LOOPING THROUGH PATH
for path_sim in ${path_multiple_dir[@]}; do
    echo "Sim path: ${path_sim}"
    ## STOPPING IF DOES NOT EXIST
    stop_if_does_not_exist "${path_sim}"

    ## LOOPING THROUGH DIRECTORIES
    for current_directory_path in $(ls ${path_sim}/*/ -d); do
        echo "--> Working on: ${current_directory_path}"

        ## GOING INTO CURRENT DIRECTORY
        cd "${current_directory_path}"

        ## CREATING LOG FILE AND ERROR FILE
        mkdir -p LOGFILE
        mkdir -p ERRORFILE

        ## COPYING OVER SUBMISISON
        cp -r "${path_chtc_sub}" "${output_sub}"
        cp -r "${path_chtc_run}" "${current_directory_path}"
        cp -r "${path_chtc_rc}" "${current_directory_path}"

        ## FINDING INSTANCE NAME
        instance_name="$(head -n1 ${instance_text_file})"
        ## SUBTRACTING 1 TO COUNT FROM 0
        length_cross_valid=$(head -n1 "${initialize_summary}" | awk '{print $NF}')

        ## IF LENGTH IS ZERO, SET TO ONE
        if [[ "${length_cross_valid}" == 0 ]]; then
            length_cross_valid="1"
        fi

        ## DEFINING ARRAY OF SCRIPTS TO TRANSFER
        declare -a transfer_inputs=("scripts" \
                                    "${chtc_rc}" \
                                    "extract_deep_cnn_separated_instances.sh" \
                                    "${run_script}" \
                                    "${instance_name}" \
                                    "indices.pickle"
                                    )
        ## ADDING SQUID TO ARRAY
        if [[ "${want_gpu}" != true ]]; then
            transfer_inputs=("http://proxy.chtc.wisc.edu/SQUID/chtc/python36.tar.gz" \
                             "http://proxy.chtc.wisc.edu/SQUID/akchew/packages.tar.gz" \
                             "${transfer_inputs[@]}")
        fi


        ## DEFINING DELIMINATOR
        transfer_inputs_as_string=$(join_by , "${transfer_inputs[@]}")

        ## EDITING SUBMISSION
        sed -i "s#_NUMCPU_#${request_cpu}#g" "${output_sub}"
        sed -i "s#_NUMMEM_#${request_memory}#g" "${output_sub}"
        sed -i "s#_DISKSPACE_#${request_disk}#g" "${output_sub}"
        sed -i "s#_NUMQUEUE_#${length_cross_valid}#g" "${output_sub}"
        sed -i "s#_RUNSCRIPT_#${run_script}#g" "${output_sub}"
        sed -i "s#_TRANSFERINPUTS_#${transfer_inputs_as_string}#g" "${output_sub}"

        if [[ "${want_gpu}" == true ]]; then
            sed -i "s#_GPULENGTH_#${gpu_length}#g" "${output_sub}"
        fi

        ## ADDING TO JOB LIST
        echo "${current_directory_path}${output_sub}" >> "${job_list}"

    done

done


