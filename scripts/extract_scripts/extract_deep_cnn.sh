#!/bin/bash
# extract_deep_cnn.sh
# This code runs the combining array python script
# VARIABLES:
#   _PYTHONSCRIPT_ <-- python path
#   _MASSFRAC_ <-- mass fraction data
#   _COSOLVENT_ <-- cosolvent data
#   _SOLUTES_  <-- solute data
#   _REPTYPE_ <-- representation type
#   _REPINPUT_ <-- representation input
#   _NUMEPOCHS_ <-- number of epochs
#   _DATATYPE_ <-- data type
#   _CNNTYPE_ <-- CNN type
#   _SAMPLETYPE_ <-- sampling type
#   _SAMPLEINPUTS_ <-- sampling inputs
#   database paths
#       _DATABASE_ <-- database path
#       _CSVDATA_ <-- csv data
#       _COMBINED_ <-- combined data sets
#       _OUTPUT_ <-- output path
#       _RESULTS_ <-- results path
#   _NUMCROSSFOLD_ <-- number of cross folding trainings

# Written by: Alex K. Chew (04/22/2019)

## USAGE EXAMPLE: bash extract_deep_cnn.sh

## DEFINING PYTHON SCRIPT
python_script="_PYTHONSCRIPT_"

## DEFINING REPRESENTATION TYPE
representation_types="_REPTYPE_" # split_avg_nonorm split_average
representation_inputs="_REPINPUT_" # Number of splits

## DEFINING SAMPLING TYPE
sampling_type="_SAMPLETYPE_"
sampling_inputs="_SAMPLEINPUTS_"

## DEFINING MASS FRACTION
mass_frac="_MASSFRAC_" # 75
cosolvents="_COSOLVENT_" # DIO,GVL,THF
solutes="_SOLUTES_"

## DEFINING MODEL
cnn_type="_CNNTYPE_" # solvent_net, orion, voxnet
epochs="_NUMEPOCHS_"
data_type="_DATATYPE_"

## DEFINING LOGICALS
want_descriptor="_WANTDESCRIPTOR_" # --want_descriptors

## DEFINING PATHS
database="_DATABASE_"
csv_data="_CSVDATA_"
combined_database_path="_COMBINED_"
output_path="_OUTPUT_"
result_path="_RESULTS_"

## DEFINING NUMBER OF CROSS FOLDERS
num_cross_fold="_NUMCROSSFOLD_"

## RUNNING PYTHON SCRIPT
python -u ${python_script} -v \
                -s "${solutes}" \
                -x "${cosolvents}" \
                -m "${mass_frac}" \
                -r "${representation_types}" \
                -g "${representation_inputs}" \
			    -d "${database}" \
                -c "${csv_data}" \
                -a "${combined_database_path}" \
                -o "${output_path}" \
                -q "${cnn_type}" \
                -n "${epochs}" \
                -z "${data_type}" \
    			-w "${result_path}" \
                --samplingtype "${sampling_type}" \
                --samplinginputs "${sampling_inputs}" \
                -b \
                "${want_descriptor}" \
                --num_cross_folds "${num_cross_fold}"
                        
                        
