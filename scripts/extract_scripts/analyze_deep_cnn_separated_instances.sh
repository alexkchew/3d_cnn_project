#!/bin/bash
# analyze_deep_cnn_separated_instances.sh
# This code analyzes deep CNN code. Note that 
# it is similar to extract_deep_cnn_separated_instances.sh 
# except that it does not include cross validation indices.
# VARIABLES:
#   _PYTHONSCRIPT_ <-- python path
#   _NUMEPOCHS_ <-- number of epochs
#   _DATATYPE_ <-- data type
#   _CNNTYPE_ <-- CNN type
#   _SAMPLETYPE_ <-- sampling type
#   _SAMPLEINPUTS_ <-- sampling inputs
#   _NUMCROSSFOLD_ <-- number of cross folding trainings
#   _RESULTSPICKLE_ <-- results pickle
#   _REWRITE_ <-- rewrite

# Written by: Alex K. Chew (03/03/2020)

## USAGE EXAMPLE: bash extract_deep_cnn.sh
## DESCRIBING CURRENT SCRIPT PATH
main_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING PYTHON SCRIPT
python_script="_PYTHONSCRIPT_"

## DEFINING PICKLE NAME
instance_pickle_name="_INSTANCEPICKLENAME_"

## DEFINING PICKLES FOR INDICES
indices_pickle="_INDICESPICKLE_"

## DEFINING SAMPLING TYPE
sampling_type="_SAMPLETYPE_"
sampling_inputs="_SAMPLEINPUTS_"

## DEFINING MODEL
cnn_type="_CNNTYPE_" # solvent_net, orion, voxnet
epochs="_NUMEPOCHS_"

## DEFINING LOGICALS
want_descriptor="_WANTDESCRIPTOR_" # --want_descriptors

## DEFINING NUMBER OF CROSS FOLDERS
num_cross_fold="_NUMCROSSFOLD_"

## DEFINING RESULTS PICKLE
results_pickle="_RESULTSPICKLE_"

## DEFINING REWRITE
rewrite="_REWRITE_"

## DEFINING PATH TO PICKLE
path_pickle="${main_path}/${results_pickle}"

## SEEING IF EXISTING OR REWRITING
if [[ ! -e "${path_pickle}" ]] || [[ "${rewrite}" == true ]]; then

## RUNNING PYTHON SCRIPT
python -u ${python_script} -v \
                --output_path "${main_path}" \
                -q "${cnn_type}" \
                -n "${epochs}" \
                --samplingtype "${sampling_type}" \
                --samplinginputs "${sampling_inputs}" \
                -b \
                "${want_descriptor}" \
                --num_cross_folds "${num_cross_fold}" \
                --indices_pickle "${indices_pickle}" \
                --pickle_name "${instance_pickle_name}" \
                --results_pickle "${results_pickle}" \
                --no_augment \
                --rewrite "${rewrite}"

else
    echo "Since pickle file exists, skipping analysis at: ${main_path}"
fi