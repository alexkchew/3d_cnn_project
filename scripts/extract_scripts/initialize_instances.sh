#!/bin/bash
# 
# initialize_instances.sh
# The purpose of this script is to generate instances if necessary. We can then 
# use the instances to copy over files and run subsequent deep CNN. Note that 
# the goal of this script is simply to generate initial instance that is then copied 
# over to the current directory.
# 
# Written by: Alex K. Chew (02/21/2020) 

## DESCRIBING CURRENT SCRIPT PATH
main_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING PYTHON SCRIPT
python_naming_script="_PYTHONNAME_"
python_instances_script="_PYTHONINSTANCES_"
python_indices_script="_PYTHONINDICES_"

## DEFINING OUTPUT NAME
output_instance_name="_OUTPUT_INSTANCE_NAME_"

## DEFINING REPRESENTATION TYPE
representation_types="_REPTYPE_" # split_avg_nonorm split_average
representation_inputs="_REPINPUT_" # Number of splits

## DEFINING DATA TYPE
data_type="_DATATYPE_"

## DEFINING MASS FRACTION
mass_frac="_MASSFRAC_" # 75
cosolvents="_COSOLVENT_" # DIO,GVL,THF
solutes="_SOLUTES_"

## DEFINING PATHS
database="_DATABASE_"
csv_data="_CSVDATA_"
combined_database_path="_COMBINED_"

## DEFINING INDICES
output_indices="_OUTPUTINDICESPICKLE_"

## DEFINING NUMBER OF CROSS FOLDERS
num_cross_fold="_NUMCROSSFOLD_"

## DEFINING PATH TO OUTPUT PICKLE
output_path="${main_path}"

## RUNNING PYTHON SCRIPT TO GET THE NAME
python -u "${python_naming_script}" \
                -s "${solutes}" \
                -x "${cosolvents}" \
                -m "${mass_frac}" \
                -r "${representation_types}" \
                -g "${representation_inputs}" \
                -z "${data_type}" \
                --path_output "${output_instance_name}"

## GETTING INSTANCE NAMES
instance_name="$(head -n1 ${output_instance_name})"
echo "${instance_name}"
## DEFINING PATH TO INSTANCES
path_instances="${script_path}/${instance_name}"

## RUNNING INSTANCES
if [ ! -e "${path_instances}" ]; then

    ## RUNNING PYTHON SCRIPT
    python -u ${python_instances_script} \
                                         --pickle_name "${instance_name}" \
                                         --output_path "${output_path}" \
                                         -d "${database}" \
                                         -c "${csv_data}" \
                                         -a "${combined_database_path}" \
                                         --skip_loading \
                                         -v
                                         
                                         

fi

## GENERATING INDICES
path_index_pickle="${script_path}/${output_indices}.pickle"

## RUNNING INDICES
if [ ! -e "${path_index_pickle}" ]; then
    ## GENERATING INDICES
    python -u "${python_indices_script}" --output_path "${output_path}" \
                                         --instances_pickle "${instance_name}" \
                                         --indices_prefix "${output_indices}" \
                                         --num_cross_folds "${num_cross_fold}"

fi

