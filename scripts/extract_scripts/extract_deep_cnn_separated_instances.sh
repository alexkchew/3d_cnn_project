#!/bin/bash
# extract_deep_separated_instances.sh
# This code runs the combining array python script
# VARIABLES:
#   _PYTHONSCRIPT_ <-- python path
#   _NUMEPOCHS_ <-- number of epochs
#   _DATATYPE_ <-- data type
#   _CNNTYPE_ <-- CNN type
#   _SAMPLETYPE_ <-- sampling type
#   _SAMPLEINPUTS_ <-- sampling inputs
#   database paths
#       _DATABASE_ <-- database path
#       _CSVDATA_ <-- csv data
#       _COMBINED_ <-- combined data sets
#       _OUTPUT_ <-- output path
#       _RESULTS_ <-- results path
#   _NUMCROSSFOLD_ <-- number of cross folding trainings

# Written by: Alex K. Chew (04/22/2019)

## USAGE EXAMPLE: bash extract_deep_cnn.sh
## DESCRIBING CURRENT SCRIPT PATH
main_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CROSS VALIDATION INDEX
cross_valid_index="${1:-0}"

## DEFINING PYTHON SCRIPT
python_script="_PYTHONSCRIPT_"

## DEFINING PICKLE NAME
instance_pickle_name="_INSTANCEPICKLENAME_"

## DEFINING PICKLES FOR INDICES
indices_pickle="_INDICESPICKLE_"

## DEFINING SAMPLING TYPE
sampling_type="_SAMPLETYPE_"
sampling_inputs="_SAMPLEINPUTS_"

## DEFINING MODEL
cnn_type="_CNNTYPE_" # solvent_net, orion, voxnet
epochs="_NUMEPOCHS_"

## DEFINING LOGICALS
want_descriptor="_WANTDESCRIPTOR_" # --want_descriptors

## DEFINING NUMBER OF CROSS FOLDERS
num_cross_fold="_NUMCROSSFOLD_"

## RUNNING PYTHON SCRIPT
python -u ${python_script} -v \
                --output_path "${main_path}" \
                -q "${cnn_type}" \
                -n "${epochs}" \
                --samplingtype "${sampling_type}" \
                --samplinginputs "${sampling_inputs}" \
                -b \
                "${want_descriptor}" \
                --num_cross_folds "${num_cross_fold}" \
                --indices_pickle "${indices_pickle}" \
                --cross_valid_index "${cross_valid_index}" \
                --pickle_name "${instance_pickle_name}"
                        
                        
