#!/bin/bash
# loop_grid_interpolation.sh
# This code uses the looping algorithm for the grid inteprolation

# Written by: Alex K. Chew (03/07/2019)

## USAGE EXAMPLE: ./bayes tic-tac-toe_sub_train.json tic-tac-toe_sub_test.json n > outfile.txt

# alias python3=/usr/bin/python3.4


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## DEFINING PYTHON SCRIPT
python_script="${CNN_PYTHON_CODES}/loop_grid_interpolation.py"

######################################
### DEFINING MAIN GRO AND XTC FILE ###
######################################

# GRO XTC FILE
gro_file="mixed_solv_prod.gro"
xtc_file="mixed_solv_prod_first_20_ns_centered_with_10ns.xtc" # Using first 20 ns of data
xtc_file="mixed_solv_prod_first_2_ns_centered.xtc" # Using first 2 ns centered
xtc_file="mixed_solv_prod_0ns_to_200ns_pbcmol.xtc" # 200 ns of data
# xtc_file="mixed_solv_prod_first_100_ns_centered_with_10ns.xtc"
# ---

## DEFINING DATABASE PREFIX
# database_prefix="20_20_20_20ns_oxy_3chan"
# 32_32_32_20ns
# "20_20_20_20ns"
# "32_32_32_20ns"
# "16_16_16_20ns"
# "20_20_20_20ns"
# "20_20_20_32ns"

## DEFINING INPUT VARIABLES
# TRAJ LOCATION
#database_name="20_20_20_50ns"
#database_name="20_20_20_10ns_allatomwithsoluteoxygen"
#database_name="20_20_20_10ns_updated"
#database_name="20_20_20_50ns_updated"
#database_name="20_20_20_100ns_updated"
#database_name="20_20_20_40ns_first"
#database_name="20_20_20_32ns_first_new_MeCN"
## ---- UPDATED DATBASES ---- #
#database_name="20_20_20_32ns_first"


## DEFINING TYPE
# data_type="All"
# "All" 
## DEFINING LIST
declare -a data_list=("DMSO" "ACE" "ACN") 
# "All"
# "All" 
# "FRU_HMF_DMSO"
#  "DMSO" "ACE" "ACN"
 # "DMSO" "ACE" "ACN"

## LOOPING THROUGH
for data_type in "${data_list[@]}"; do

############################
### LOOP GRIDING DETAILS ###
############################

# BOX INFORMATION
box_size=4.0
box_inc=0.2 # 20 x 20 x 20
# box_inc=0.25 # 16 x 16 x 16
# box_inc=0.125 # 32 x 32 x 32

## DEFINING MAPPING TYPE
box_map_type="3channel_oxy"
# "4chan_hydroxyl"
# "3channel_hydroxyl"
# "solvent_only"
# "3channel_hydroxyl"
# "solvent_only"
# "3channel_oxy" <-- selected
# "allatom"
# "allatomwithsoluteoxygen" # <-- make sure to change to allatom if you don't want oxygen
# allatom
# allatomwithsoluteoxygen

## NORMALIZATION
normalization="maxima" # maxima rdf

## DEFINING DATABASE PREFIX
if [[ "${box_inc}" == 0.2 ]]; then
   database_prefix="20_20_20"
elif [[ "${box_inc}" == 0.25 ]]; then
   database_prefix="16_16_16"
elif [[ "${box_inc}" == 0.125 ]]; then
   database_prefix="32_32_32"
else
   echo "Error! Dataprefix is not defined for box increment: ${box_inc}"
   exit 1
fi

## ADDING BASED ON TIME
if [[ "${xtc_file}" == "mixed_solv_prod_first_20_ns_centered_with_10ns.xtc" ]]; then
   database_prefix="${database_prefix}_20ns"
elif [[ "${xtc_file}" == "mixed_solv_prod_first_100_ns_centered_with_10ns.xtc" ]]; then
   database_prefix="${database_prefix}_110ns" 
elif [[ "${xtc_file}" == "mixed_solv_prod_first_2_ns_centered.xtc" ]]; then
   database_prefix="${database_prefix}_2ns" 
elif [[ "${xtc_file}" == "mixed_solv_prod_0ns_to_200ns_pbcmol.xtc" ]]; then
   database_prefix="${database_prefix}_200ns" 
else
   echo "Error! Prefix not defined for xtc file: ${xtc_file}"
   exit 1
fi

## ADDING BASED ON MAPPING TYPE
if [[ "${box_map_type}" == "3channel_oxy" ]]; then
   database_prefix="${database_prefix}_oxy_3chan"
elif [[ "${box_map_type}" == "allatomwithsoluteoxygen" ]]; then
   database_prefix="${database_prefix}_oxy"
else
    database_prefix="${database_prefix}_${box_map_type}"
   # echo "Error! Prefix not defined for map type: ${box_map_type}"
   # exit 1
fi

## DEFINING DETAILS OF SIMULATIONS
if [[ "${data_type}" == "All" ]]; then
   input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/170814-7Molecules_200ns_Full"
   mass_frac="10,25,50,75" # 75
   solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"
   cosolvents="DIO,GVL,THF" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
   #
   database_name="${database_prefix}_firstwith10" # _firstwith10"
   temperatures="403.15,343.15,373.15,403.15,433.15,403.15,363.15"
elif [[ "${data_type}" == "DMSO" ]]; then
   ##### FOR NATURE CATALYSIS DMSO WORK
   input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/SOLVENTNET_DMSO"
   # 190305-TBA_FRU_MIXED_SOLV_DMSO"
   mass_frac="10,25,50,75" # 75
   solutes="FRU,PDO,tBuOH"
   cosolvents="dmso" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THFq
   temperatures="373.15,433.15,363.15"
   database_name="${database_prefix}_firstwith10-TESTSET" # dmso"
elif [[ "${data_type}" == "ACE" ]]; then
   ###### FOR FRUCTOSE ALI'S WORK
   input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/SOLVENTNET_ACE"
   # 190612-FRU_HMF_Run"
   mass_frac="12,35,56,75" # 75
   cosolvents="ACE" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
   solutes="FRU,GLU"
   temperatures="393.15,393.15"
   database_name="${database_prefix}_firstwith10-TESTSET" # "${database_prefix}_firstwith10_ACE"
elif [[ "${data_type}" == "ACN" ]]; then
   ###### FOR NAT CATALYSIS MECN WORK
   input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/SOLVENTNET_MECN"
   # 190614-Run_MeCN"
   mass_frac="10,25,50,75" # 75
   cosolvents="ACN" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
   solutes="FRU,PDO,tBuOH" # "FRU,GLU"
   temperatures="373.15,393.15,363.15"
   database_name="${database_prefix}_firstwith10-TESTSET"  # "${database_prefix}_firstwith10_ACN"
elif [[ "${data_type}" == "FRU_HMF_DMSO" ]]; then
    ### FOR FRU AND HMF IN DMSO
   input_traj_loc="/home/akchew/scratch/SideProjectHuber/Analysis/190925-2ns_mixed_solvent_with_FRU_HMF/"
   mass_frac="2,5,10,25,50,75"
   cosolvents="dmso" #  DIO,GVL,THF ,dmso ,GVL,THF ,dmso DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso ,GVL,THF
   solutes="FRU,HMF" # "FRU,GLU"
   temperatures="403.15,403.15"
   database_name="${database_prefix}_FRU_HMF_DMSO"
else
   echo "Error! Data type not defined for ${data_type}"
   exit 1
fi

# -----------------------
# DESIRED DATABASE LOCATION
output_database_loc="${CNN_DATABASE}/${database_name}"

## MAKING DIRECTORY
mkdir -p "${output_database_loc}"

## PRINTING
echo "--- Creating database: ${output_database_loc} ---"
echo "Datatype: ${data_type}"
echo "Solutes: ${solutes}"
echo "Temperatures: ${temperatures}"
echo "Mass fraction: ${mass_frac}"
echo "Database name: ${database_name}"
echo "Trajectory location: ${input_traj_loc}"
echo "XTC file: ${xtc_file}"
sleep 3
## RUNNING PYTHON SCRIPT
# python3 
# /usr/bin/python3.4
/usr/bin/python3.4 "${python_script}" -i "${input_traj_loc}" \
                                    -o "${output_database_loc}" \
                                    -g "${gro_file}" \
                                    --xtcfile "${xtc_file}" \
                                    -c "${box_inc}" \
                                    -b "${box_size}" \
                                    -t "${box_map_type}" \
                                    -n "${normalization}" \
                                    -m "${mass_frac}" \
                                    --solvent "${cosolvents}" \
                                    -s "${solutes}" \
                                    --temp "${temperatures}" \

done
