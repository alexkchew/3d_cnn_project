#!/bin/bash
# sampling_time_chunks.sh
#
# This script runs generate_deep_cnn.sh code for multiple sampling time with the goal of finding the first 
# frame that is most useful for us. 

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"

#####################
### DATABASE TYPE ###
#####################

## DEFINING DATABASE TYPE
#database_type="32_32"
# database_type="20_20_20_32ns_firstwith10" # _first_withoxy
# database_type="20_20_20_20ns_firstwith10"
# 20_20_20_32ns_first
# "30_30_30_32ns_first"
# 20_20_20_32ns_first
# 20_20_20_40ns_first
# 20_20_20_50ns_updated
# 20_20_20_10ns_updated
# "20_20_20_10ns_allatomwithsoluteoxygen"
# 20_20_20_190ns
# 20_20_20_withdmso
# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"
# 20_20_20_rdf
# 20_20_20_10ns_allatomwithsoluteoxygen

## DEFINING DATABASE TYPE
#database_type="32_32"
# database_type="20_20_20_110_origin" # "20_20_20_100ns_first"
database_type="20_20_20_110ns_oxy_3chan_firstwith10"
# 20_20_20_190ns
# 20_20_20_withdmso
# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"
# 20_20_20_rdf

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING CURRENT DIRECTORY
main_dir="${1-190804-solvent_net_sampling_chunks_20ns}"

## DEFINING NETWORK
#cnn_type="vgg16"
cnn_type="solvent_net"
# solvent_net
# vgg16

## DEFINING SAMPLING TIME INITIAL AND LAST FRAMES
# declare -a sampling_time_initial=("15000" "13000" "11000" "9000" "7000" "5000" "3000" "1000")
# declare -a sampling_time_final=("19000" "17000" "15000" "13000" "11000" "9000" "7000" "5000")

#declare -a sampling_time_initial=('0' '500' '1000' '1500' '2000' '2500'  '3000'  '3500'  '4000'  '4500'  '5000'  '5500'  '6000'  '6500'  '7000'  '7500')
#declare -a sampling_time_final=('3200' '3700' '4200' '4700' '5200' '5700'  '6200'  '6700'  '7200'  '7700'  '8200'  '8700'  '9200'  '9700'  '10200' '10700')

declare -a sampling_time_initial=('0'	'500'	'1000'	'1500'	'2000'	'2500'	'3000'	'3500'	'4000'	'4500'	'5000'	'5500'	'6000'	'6500'	'7000'	'7500'	'8000'	'8500'	'9000')
declare -a sampling_time_final=('2000'	'2500'	'3000'	'3500'	'4000'	'4500'	'5000'	'5500'	'6000'	'6500'	'7000'	'7500'	'8000'	'8500'	'9000'	'9500'	'10000'	'10500'	'11000')

# declare -a sampling_time_initial=('1000')
# declare -a sampling_time_final=('4200')

## DEFINING SPLIT
split_chunks="10" # "8"

## DEFINING REPRESENTATION TYPE
representation_types="split_avg_nonorm_sampling_times"
#representation_types="split_avg_nonorm_planar"
# default: split_avg_nonorm
# split_avg_nonorm_50
# split_avg_nonorm_25
# split_avg_nonorm_01
# split_avg_nonorm_planar
# Default: 5
# 10
# 1001

## DEFINING DETAILS OF SIMULATIONS
mass_frac="10,25,50,75" # 75
cosolvents="DIO,GVL,THF" # DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso
solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"

## DEFINING NUMBER OF EPOCHS
epochs="500"

## DEFINING INPUT BASH SCRIPT
path_input_bash="${CNN_SCRIPT_DIR}/generate_deep_cnn.sh"

## SAMPLING TIMES
sampling_perc="1.0"

## DEFINING SAMPLING TYPE
sampling_type="strlearn"
sampling_inputs="1.00" # "0.75" 
# sampling_inputs="0.80" # "0.75" 

## DEFINING DESCRIPTOR
want_descriptor=false # true

########################
### DEFINING OUTPUTS ###
########################

## SUBMISSION SCRIPT
output_submit="submit.sh"

## DEFINING OUTPUT
output_sim_info="train_info.out"

## DEFINING OUTPUT BASH SCRIPT
output_bash_script="train_sampling_chunks.sh"

## DEFINING OUTPUT SUBMISSION SCRIPT
output_bash_extraction_script="extract_deep_cnn.sh"

######################
### DEFINING PATHS ###
######################

## DEFINING OUTPUT PATH
path_output="${CNN_OUTPUT_DATA}/${main_dir}"

## CREATING OUTPUT DIRECTORY
create_dir "${path_output}" -f 

## PATH TO SUBMISSION SCRIPT
path_submit_script="${CNN_SUBMIT_SCRIPTS}/submit.sh"

## DEFINING PATH TO OUTPUT ABSH
path_output_bash_run="${path_output}/${output_bash_script}"

## DEFINING OUTPUT PATH
path_output_submit_script="${path_output}/${output_submit}"


####################
### MAIN SCRIPTS ###
####################
## FINDING TOTAL SAMPLING INCREMENTS
total_sampling_increments="${#sampling_time_initial[@]}"
total_sampling_increments_minus_one="$((${total_sampling_increments}-1))" # -1

## WANT SEPARATE BASH SCRIPT
want_separate_bash=true # True if you want one for each submission

## DEFINING NUMBER OF FOLDS
num_cross_valid_folds="5"

## LOOPING THROUGH SAMPLING PERC ARRAY
for sampling_index in $(seq 0 ${total_sampling_increments_minus_one}); do
    ## DEFINING INITIAL AND LAST FRAMES
    initial_frame="${sampling_time_initial[${sampling_index}]}"
    last_frame="${sampling_time_final[${sampling_index}]}"
    
    ## PRINTING
    echo "Working on frames ${initial_frame} to ${last_frame}"
    
    ## DEFINING REPRESENTATION
    representation_inputs="${initial_frame},${last_frame},${split_chunks},${sampling_perc}"
    # "${split_chunks},${sampling_perc},${initial_frame},${last_frame}" # Number of splits 19
    
    ## RUNNING BASH SCRIPT
    bash "${path_input_bash}" \
            "${main_dir}" \
            "${cnn_type}" \
            "${representation_types}" \
            "${representation_inputs}" \
            "${mass_frac}" \
            "${cosolvents}" \
            "${solutes}" \
            "${epochs}" \
            "${database_type}" \
            "${sampling_type}" \
            "${sampling_inputs}" \
            "${want_separate_bash}" \
            "${want_descriptor}" \
            "${num_cross_valid_folds}"
            
    ## DEFINING OUTPUT NAME
    output_name="$(CNN_generate_name ${representation_types}\
                                     ${representation_inputs}\
                                     ${mass_frac} ${cosolvents} ${solutes} ${epochs} ${database_type} \
                                     ${cnn_type} \
                                    "${sampling_type}" \
                                    "${sampling_inputs}" \
                                    "${want_descriptor}" \
        )"
        
    ## GETTING THE OUTPUT NAME AS A BASH SCRIPT
    echo "####" >> "${path_output_bash_run}"
    echo "echo \"Check output at: ${path_output}/${output_name}/${output_sim_info}\"" >> "${path_output_bash_run}"
    echo "cd \"${path_output}/${output_name}\"; bash ${output_bash_extraction_script} > ${output_sim_info} &" >> "${path_output_bash_run}"
    echo "" >> "${path_output_bash_run}"
    #  2>&1
    
done

## WAITING FOR ALL FILES
echo "" >> "${path_output_bash_run}"
echo "wait" >> "${path_output_bash_run}"

## COPYING SUBMISSION SCRIPT
cp "${path_submit_script}" "${path_output_submit_script}"

## EDITING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${main_dir}/g" "${path_output_submit_script}"
sed -i "s/_BASHSCRIPT_/${output_bash_script}/g" "${path_output_submit_script}"

if [[ "${want_separate_bash}" == false ]]; then
    ## ADDING TO JOB LIST
    echo "${path_output_submit_script}" >> "${CNN_JOB_LIST}"
fi

