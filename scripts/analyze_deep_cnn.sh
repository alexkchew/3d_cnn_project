#!/bin/bash
# analyze_deep_cnn.sh
# This script analyzes all deep_cnn code after completion of the CNN training.
# We assume that you have completed the generate_deep_cnn.sh script. 
# This script would have completed some 5-fold cross validation. 
# INPUTS:
#   $1: simulation name
#   $2: path to multiple simulations
# USAGE:
#	bash analyze_deep_cnn.sh MULTIPLE /home/akchew/scratch/3d_cnn_project/simulations/20200302-complete_chtc_jobs
# EXAMPLES:
#    bash analyze_deep_cnn.sh TEST_100_SAMPLING
#
# Good way to check if all jobs are complete:
#	tail -n1 */job* | less

## TO RUN:
#   load_python36
#   bash loop_analyze_deep_cnn.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

#######################
### DEFINING INPUTS ###
#######################

## DEFINING SIMULATION FOLDER
sim_folder="$1" # MULTIPLE FLAG

## DEFINING SPECIFIC DIR
path_to_multiple_sim="${2-""}"

#########################
### DEFINING DEFAULTS ###
#########################

## DEFINING REWRITE
rewrite=true

## DEFINING ANALYSIS SCRIPT
analysis_script="analyze_deep_cnn_separated_instances.sh"

## DEFINING BASH SCRIPT
extract_script="extract_deep_cnn_separated_instances.sh"

## DEFINING PATH TO ANALYSIS PYTHON SCRIPT
output_scripts="scripts"
path_python_script="${output_scripts}/analyze_deep_cnn_separated_instance.py"

## DEFINING PATHS
path_analysis_script="${CNN_EXTRACT_DIR}/${analysis_script}"

## DEFINING RESULTS PICKLE
results_pickle="model.results"

## DEFINING JOB LIST
job_list="${ANALYZE_JOB_LIST}"
error_job_list="${ANALYZE_ERR_LIST}"

## IF SIM FOLDER IS MULTIPLE, THEN IT WILL LOOP THROUGH ALL TRAJECTORIES
if [[ "${sim_folder}" == "MULTIPLE" ]]; then
    echo "Since MULTIPLE is selected, then we will loop through all potential path sims"
    
    ## PRINTING ERROR
    if [ -z "${path_to_multiple_sim}" ]; then
        echo "Error! Multiple flag is set on but no path has been included"
        exit 1
    fi
    
    ## GETTING DIRECTORIES
    if [[ "${path_to_multiple_sim}" = /* ]]; then
    	read -a path_multiple_dir <<< $(ls ${path_to_multiple_sim}/*/ -d)
    else
    	read -a path_multiple_dir <<< $(ls ${CNN_OUTPUT_DATA}/${path_to_multiple_sim}/*/ -d)
    fi

else
    ## DEFINING PATH TO SIM
    default_path_to_sim="${CNN_OUTPUT_DATA}/${sim_folder}"
    ## DEFINING PATHS
    declare -a path_multiple_dir=("${default_path_to_sim}")

fi

## LOOPING THROUGH PATH
for path_sim in ${path_multiple_dir[@]}; do
	echo "==========================="
    echo "Sim path: ${path_sim}"
    ## STOPPING IF DOES NOT EXIST
    stop_if_does_not_exist "${path_sim}"

    ## LOOPING THROUGH DIRECTORIES
    for current_directory_path in $(ls ${path_sim}/*/ -d); do
        echo "--> Working on: ${current_directory_path}"

        ## GOING INTO CURRENT DIRECTORY
        cd "${current_directory_path}"

        ## CHECKING IF FILE IS THERE
        if [[ ! -e "${extract_script}" ]]; then
        	echo "----------------------------------"
        	echo "${extract_script} does not exist!"
        	echo "Check directory in: ${current_directory_path}"
        	echo "----------------------------------"
        	echo "${current_directory_path}" >> "${error_job_list}"
        	break
        fi

        ## EXTRACTION OF SUBMISSION INFORMATION
        instance_pickle_name="$(extract_variable_name_from_file "${extract_script}" 'instance_pickle_name=')"
        indices_pickle="$(extract_variable_name_from_file "${extract_script}" 'indices_pickle=')"
        sampling_type="$(extract_variable_name_from_file "${extract_script}" 'sampling_type=')"
        sampling_inputs="$(extract_variable_name_from_file "${extract_script}" 'sampling_inputs=')"
        cnn_type="$(extract_variable_name_from_file "${extract_script}" 'cnn_type=')"
        epochs="$(extract_variable_name_from_file "${extract_script}" 'epochs=')"
        want_descriptor="$(extract_variable_name_from_file "${extract_script}" 'want_descriptor=')"
        num_cross_fold="$(extract_variable_name_from_file "${extract_script}" 'num_cross_fold=')"

		## PREFIX FOR MODEL
		if [[ "${num_cross_fold}" -eq "0" ]]; then
			default_model_prefix="model"
		else
			default_model_prefix="model_fold_"
		fi

        ## COPYING OVER ANALYSIS FILE
        cp -r "${path_analysis_script}" ./

        ## CHECKING IF JOB IS COMPLETE (Output is suppressed)
        check_model_completion job_status "${default_model_prefix}" "${num_cross_fold}" >/dev/null 2>&1

        ## CHECKING IF JOB STATUS IS FALSE
        if [[ "${job_status}" == false ]]; then
        	echo "----------------------------------"
        	echo "Job is not complete!"
        	echo "Check the job in ${current_directory_path}"
    		echo "Default model prefix: ${default_model_prefix}"
    		echo "Number of cross validation folds: ${num_cross_fold}"
        	echo "----------------------------------"
			echo "${current_directory_path}" >> "${error_job_list}"
			break
        fi

		## COPYING ALL SCRIPTS
		rsync -a "${CNN_PYTHON_CODES}/" "./${output_scripts}"
		# cp -rv "${CNN_PYTHON_CODES}" "${output_scripts}"

		## EDITING SCRIPT
		sed -i "s#_PYTHONSCRIPT_#${path_python_script}#g" "${analysis_script}"
		sed -i "s#_INSTANCEPICKLENAME_#${instance_pickle_name}#g" "${analysis_script}"
		sed -i "s#_INDICESPICKLE_#${indices_pickle}#g" "${analysis_script}"
		sed -i "s#_SAMPLETYPE_#${sampling_type}#g" "${analysis_script}"
		sed -i "s#_SAMPLEINPUTS_#${sampling_inputs}#g" "${analysis_script}"
		sed -i "s#_CNNTYPE_#${cnn_type}#g" "${analysis_script}"
		sed -i "s#_NUMEPOCHS_#${epochs}#g" "${analysis_script}"
		sed -i "s#_WANTDESCRIPTOR_#${want_descriptor}#g" "${analysis_script}"
		sed -i "s#_NUMCROSSFOLD_#${num_cross_fold}#g" "${analysis_script}"
		sed -i "s#_RESULTSPICKLE_#${results_pickle}#g" "${analysis_script}"
        sed -i "s#_REWRITE_#${rewrite}#g" "${analysis_script}"
        
		## ADDING EXTRACTION SCRIPT TO JOB LIST
		echo "${current_directory_path}${analysis_script}" >> "${job_list}"


    done

done


