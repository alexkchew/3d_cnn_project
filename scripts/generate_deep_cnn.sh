#!/bin/bash
# generate_deep_cnn.sh
# This scripts generate deep cnn script. 

# Written by: Alex K. Chew (05/10/2019)
#
# USAGE:
#   bash generate_deep_cnn.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

#################
### FUNCTIONS ###
#################

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING CURRENT DIRECTORY
main_dir="$1" # "190515-solvent_net_with_randomization_with_shuffle_rdfwithnorm"

## CHECK    ING IF MAIN DIRECTORY EXISTS
if [ -z "${main_dir}" ]; then
    echo "Error! Main directory is: ${main_dir}"
    echo "Perhaps you made an input error?"
    exit 1
fi

## DEFINING NETWORK
#cnn_type="vgg16"
cnn_type="$2" # "solvent_net"
# solvent_net
# vgg16

## DEFINING REPRESENTATION TYPE
representation_types="$3" # "split_avg_withnorm"
#representation_types="split_avg_nonorm_planar"
# split_avg_nonorm
# split_avg_nonorm_planar
representation_inputs="$4" # "5" # Number of splits

## DEFINING DETAILS OF SIMULATIONS
mass_frac="$5" # "10,25,50,75" # 75
cosolvents="$6" # "DIO" # DIO,GVL,THF ,GVL,THF ,GVL,THF
solutes="$7" # "CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"

## DEFINING NUMBER OF EPOCHS
epochs="$8" # "500"

## DEFINING DATABASE TYPE
#database_type="32_32"
database_type="$9" # "20_20_20_rdf"

## DEFINING SAMPLING INPUTS
sampling_type="${10}"

## DEFINING INPUTS FOR SAMPLING TYPE
sampling_inputs="${11}"

## DEFINING IF YOU WANT TO ADD TO JOB LIST
add_job_list="${12:-true}"

## DEFINING IF YOU WANT DESCRIPTORS
want_descriptors="${13:-false}"

## DEFINING IF YOU WANT CROSS VALIDATION FOLDS
num_cross_valid_folds="${14-1}"

## DEFINING LOGICAL IF YOU WANT 
want_separate_jobs=true

## DEFINING IF YOU WANT A SUBMISSION SCRIPT WITH ALL CODES RUN IN PARALLEL
want_single_parallel_job="${15-true}"

# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"

## DEFINING YOUR PARENT PATH IS DIFFERENT
parent_path="${16-${CNN_OUTPUT_DATA}}"

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT BASH SCRIPT
## DEFINING PATH TO EXTRACT DEEP CNN
if [[ "${want_separate_jobs}" == true ]]; then
    path_input_bash="${CNN_EXTRACT_DIR}/extract_deep_cnn_separated_instances.sh"
    ## PATH TO SUBMISSION SCRIPT
    path_submit_script="${CNN_SUBMIT_SCRIPTS}/submit_single_cross_valid.sh"
else
    path_input_bash="${CNN_EXTRACT_DIR}/extract_deep_cnn.sh"
    ## PATH TO SUBMISSION SCRIPT
    path_submit_script="${CNN_SUBMIT_SCRIPTS}/submit.sh"
fi
## DEFINING INSTANCES BASH SCRIPT
path_instances_bash="${CNN_EXTRACT_DIR}/initialize_instances.sh"

### DEFINING TYPES OF DATABASES
if [ "${database_type}" == "32_32" ]; then
    database_type_listed_name="32_32_32"
else
    database_type_listed_name=${database_type}
fi

## DATABASE
path_database="${CNN_DATABASE}/${database_type_listed_name}"
## DEFINING REGRESSION DATA
path_csv_data="${CNN_EXP_DATA}/solvent_effects_regression_data.csv"
## DEFINING PATH TO COMBINED DATA
path_combined_data="${CNN_COMBINED_DATA}"


########################
### DEFINING OUTPUTS ###
########################

## DEFINING OUTPUT NAME
output_name="$(CNN_generate_name ${representation_types} \
                                 ${representation_inputs}\
                                 ${mass_frac} ${cosolvents} \
                                 ${solutes} \
                                 ${epochs} \
                                 ${database_type} \
                                 ${cnn_type} \
                                 ${sampling_type} \
                                 ${sampling_inputs} \
                                 ${want_descriptors} \
    )"
echo "${output_name}"

## DEFINING OUTPUT PATH
path_output="${parent_path}/${main_dir}/${output_name}"

## DEFINING OUTPUT SUBMISSION SCRIPT
output_submit="submit.sh"
output_bash_script="$(basename ${path_input_bash})"
output_instances_script="$(basename ${path_instances_bash})"

## DEFINING OUTPUT SCRIPT
output_scripts="scripts"

## DEFINING PYTHON SCRIPT
path_naming_instances_script="${output_scripts}/output_instance_name.py"
path_instances_script="${output_scripts}/combining_arrays.py"
path_indices_script="${output_scripts}/generate_cross_validation_fold_indices.py"
## PYTHON SCRIPT FOR EXTRACTION
if [[ "${want_separate_jobs}" == true ]]; then
    path_python_script="${output_scripts}/extract_deep_cnn_separated_instances.py"
else
    path_python_script="${output_scripts}/extract_deep_cnn.py"
fi

## DEFINING OUTPUT INSTANCE
output_instance_name="instance_name.txt"
output_indicies_pickle="indices" # .pickle

###################
### MAIN SCRIPT ###
###################

## CREATING OUTPUT DIRECTORY
create_dir "${path_output}" -f 

## GOING INTO DIRECTORY
cd "${path_output}"

###################
### BASH SCRIPT ###
###################

## COPYING ALL SCRIPTS
cp -r "${CNN_PYTHON_CODES}" "${output_scripts}"

## COPYING BASH SCRIPT
cp -r "${path_input_bash}" "${output_bash_script}"
cp -r "${path_instances_bash}" "${output_instances_script}"

## EDITING INSTANCES SCRIPT
sed -i "s#_PYTHONNAME_#${path_naming_instances_script}#g" "${output_instances_script}"
sed -i "s#_PYTHONINSTANCES_#${path_instances_script}#g" "${output_instances_script}"
sed -i "s#_OUTPUT_INSTANCE_NAME_#${output_instance_name}#g" "${output_instances_script}"
sed -i "s#_PYTHONINDICES_#${path_indices_script}#g" "${output_instances_script}"
sed -i "s#_OUTPUTINDICESPICKLE_#${output_indicies_pickle}#g" "${output_instances_script}"

## EDITING BASH SCRIPT
sed -i "s/_MASSFRAC_/${mass_frac}/g" "${output_bash_script}" "${output_instances_script}"
sed -i "s/_COSOLVENT_/${cosolvents}/g" "${output_bash_script}" "${output_instances_script}"
sed -i "s/_SOLUTES_/${solutes}/g" "${output_bash_script}" "${output_instances_script}"
sed -i "s/_REPTYPE_/${representation_types}/g" "${output_bash_script}" "${output_instances_script}"
sed -i "s/_REPINPUT_/${representation_inputs}/g" "${output_bash_script}" "${output_instances_script}"

sed -i "s/_NUMEPOCHS_/${epochs}/g" "${output_bash_script}"
sed -i "s/_DATATYPE_/${database_type}/g" "${output_bash_script}" "${output_instances_script}"
sed -i "s/_CNNTYPE_/${cnn_type}/g" "${output_bash_script}"
## UPDATING PATHS
sed -i "s#_PYTHONSCRIPT_#${path_python_script}#g" "${output_bash_script}"
sed -i "s#_DATABASE_#${path_database}#g" "${output_bash_script}" "${output_instances_script}"
sed -i "s#_CSVDATA_#${path_csv_data}#g" "${output_bash_script}" "${output_instances_script}"
sed -i "s#_COMBINED_#${path_combined_data}#g" "${output_bash_script}" "${output_instances_script}"
sed -i "s#_OUTPUT_#${path_output}#g" "${output_bash_script}"
sed -i "s#_RESULTS_#${path_output}#g" "${output_bash_script}"
## UPDATING SAMPLING INFORMATION
sed -i "s/_SAMPLETYPE_/${sampling_type}/g" "${output_bash_script}"
sed -i "s/_SAMPLEINPUTS_/${sampling_inputs}/g" "${output_bash_script}"
## UPDATING DESCRIPTORS
if [[ "${want_descriptors}" == true ]]; then
    sed -i "s/_WANTDESCRIPTOR_/--want_descriptors/g" "${output_bash_script}"
else
    sed -i "s/_WANTDESCRIPTOR_//g" "${output_bash_script}"
fi
## CROSS VALIDATION
sed -i "s/_NUMCROSSFOLD_/${num_cross_valid_folds}/g" "${output_bash_script}" "${output_instances_script}"

## RUNNING INSTANCE SCRIPT
bash "${output_instances_script}"

## DEFINING OUTPUT SUMMARY
initialize_summary="${output_indicies_pickle}.summary"
initialize_pickle="${output_indicies_pickle}.pickle"
## GETTING TOTAL CROSS VALIDATION
length_cross_valid=$(head -n1 "${initialize_summary}" | awk '{print $NF}')
## SUBTRACTING 1 TO COUNT FROM 0
if [[ "${length_cross_valid}" -gt 0 ]]; then 
    length_cross_valid=$(( ${length_cross_valid} - 1 ))
else
    length_cross_valid=0
fi


## GETTING INSTANCEN AMES
instance_name="$(head -n1 ${output_instance_name})"

## EDITING
sed -i "s#_INSTANCEPICKLENAME_#${instance_name}#g" "${output_bash_script}"
sed -i "s#_INDICESPICKLE_#${initialize_pickle}#g" "${output_bash_script}"

## ADDING RUN SCRIPT
run_prefix="run"

## GENERATING RUN SCRIPTS
for idx in $(seq 0 ${length_cross_valid}); do
    ## DEFINING 
    current_run_script="${run_prefix}_${idx}.sh"
    ## ADDING HEADER
    echo "#!/bin/bash" > "${current_run_script}"
    ## ADDING RUN SCRIPT
    echo "" >> "${current_run_script}"
    echo "bash ${output_bash_script} ${idx}" >> "${current_run_script}"
done

#########################
### SUBMISSION SCRIPT ###
#########################

## FOR SINGLE JOB
if [[ "${want_single_parallel_job}" == true ]]; then
    single_submit_job_name="submit_single_parallel.sh"
    cp "${path_submit_script}" "${single_submit_job_name}"
fi


if [[ "${want_separate_jobs}" == true ]]; then
    for idx in $(seq 0 ${length_cross_valid}); do
        ## LOOP TO GET SUBMISSION
        output_submit_name="${output_submit%.sh}_${idx}.sh"
        ## DEFINING 
        current_run_script="${run_prefix}_${idx}.sh"
        ## COPYING SUBMISSION SCRIPT
        cp "${path_submit_script}" "${output_submit_name}"
        ## ADDING TO CODE
        echo "bash ${current_run_script}" >> "${output_submit_name}"
        
        ## EDITING JOB NAME
        sed -i "s/_JOB_NAME_/${output_name}_${idx}/g" "${output_submit_name}"
        
        if [[ "${want_single_parallel_job}" == true ]]; then
            ## ADDING TO THE SUBMISSION JOB
            echo "bash ${current_run_script} &"  >> "${single_submit_job_name}"
        fi
        
        ## CREATING A JOB LIST
        if [[ "${add_job_list}" == true ]]; then
                echo "${path_output}/${output_submit_name}" >> "${CNN_JOB_LIST}"
        fi
    
    done
    
    ## ADDING THE FINAL TOUCHES
    if [[ "${want_single_parallel_job}" == true ]]; then
        ## EDITING JOB NAME
        sed -i "s/_JOB_NAME_/${output_name}_single_parallel/g" "${single_submit_job_name}"
        echo "wait" >> "${single_submit_job_name}"
        echo "${path_output}/${single_submit_job_name}" >> "${CNN_JOB_LIST}"
    fi
else

    ## COPYING SUBMISSION SCRIPT
    cp "${path_submit_script}" "${output_submit}"

    ## EDITING SUBMISSION SCRIPT
    sed -i "s/_JOB_NAME_/${output_name}/g" "${output_submit}"
    sed -i "s/_BASHSCRIPT_/${output_bash_script}/g" "${output_submit}"

    ## CREATING A JOB LIST
    if [ "${add_job_list}" == true ]; then
        echo "${path_output}/${output_submit}" >> "${CNN_JOB_LIST}"
    fi

fi
