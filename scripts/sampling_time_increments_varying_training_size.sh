#!/bin/bash
# sampling_time_increments_varying_training_size.sh
#
# This scripts runs generate_deep_cnn.sh code for multiple sampling times and 
# varying training sizes. For instance, suppose you had a 100 ns trajectory. 
# You would like to see if we can vary the training size (e.g. 10 ns) or the 
# number of training splits (8 vs. 3). We make the following assumptions:
#   - max time for a single trajectory to average is about 10 ns
#   - We use percentage of 10 ns as a way of computing averages
#   - testing set always remains the same (e.g 2 testing)
# 
# Written by: Alex K. Chew (06/06/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING CURRENT DIRECTORY
main_dir="${1-20200210-sampling_inc_varying}"
# "190804-solvent_net_sample_increment_varying_training_size_3chan"

## DEFINING NETWORK
#cnn_type="vgg16"
cnn_type="solvent_net"
# solvent_net
# vgg16

## DEFINING SAMPLING TIME INCREMENTS
declare -a sampling_perc_array=("0.05" "0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" "0.8" "0.9" "1.0")

## DEFINING TRAINING SIZE INCREMENTS
declare -a training_size_increments=("1" "6")
# "10"
# "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11"

 #"1" "2" "3" "4" "5" "6" "8" 

## DEFINING TEST SIZE
test_size="0"

# #### OLD VERSION ####
# ## DEFINING TRAINING SIZE INCREMENTS
# declare -a training_size_increments=("1" "2" "3" "4" "5" "6" "7" "8" "9") #"1" "2" "3" "4" "5" "6" "8" 

# ## DEFINING TEST SIZE
# test_size="2"


## DEFINING INITIAL FRAME
initial_frame="0"

## DEFINING TOTAL FRAMES
last_frame="11000" # 10000 -- 100 ns from the end

## DEFINING SPLIT
split_chunks="11" # 10

## DEFINING REPRESENTATION TYPE
representation_types="split_avg_nonorm_sampling_times"
#representation_types="split_avg_nonorm_planar"
# default: split_avg_nonorm
# split_avg_nonorm_50
# split_avg_nonorm_25
# split_avg_nonorm_01
# split_avg_nonorm_planar
# Default: 5
# 10
# 1001

## DEFINING SAMPLING TYPE
sampling_type="spec_train_tests_split"

## DEFINING DETAILS OF SIMULATIONS
mass_frac="10,25,50,75" # 75
cosolvents="DIO,GVL,THF" # DIO,GVL,THF ,GVL,THF ,GVL,THF ,dmso
solutes="CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH"

## DEFINING NUMBER OF EPOCHS
epochs="500"

## DEFINING DATABASE TYPE
#database_type="32_32"
# database_type="20_20_20_110_origin"
database_type="20_20_20_110ns_oxy_3chan_firstwith10"
# 20_20_20_100ns_first
# 20_20_20_110_origin
# 20_20_20_100ns_updated
# 20_20_20_50ns
# 20_20_20_190ns
# 20_20_20_withdmso
# "20_20_20"
# "32_32_32"
# "32_32"
# "30_30_30"
# 20_20_20_rdf

## DEFINING INPUT BASH SCRIPT
path_input_bash="${CNN_SCRIPT_DIR}/generate_deep_cnn.sh"

## DEFINING DESCRIPTOR
want_descriptor=false # true

## DEFINING NUMBER OF FOLDS
num_cross_valid_folds="5"

########################
### DEFINING OUTPUTS ###
########################

## SUBMISSION SCRIPT
output_submit="submit.sh"

## DEFINING OUTPUT
output_sim_info="train_info.out"

## DEFINING OUTPUT BASH SCRIPT
output_bash_script="train_sampling_increments.sh"

## DEFINING OUTPUT SUBMISSION SCRIPT
output_bash_extraction_script="extract_deep_cnn.sh"

######################
### DEFINING PATHS ###
######################

## DEFINING OUTPUT PATH
path_output="${CNN_OUTPUT_DATA}/${main_dir}"

## CREATING OUTPUT DIRECTORY
create_dir "${path_output}" -f 

## PATH TO SUBMISSION SCRIPT
path_submit_script="${CNN_SUBMIT_SCRIPTS}/submit.sh"

####################
### MAIN SCRIPTS ###
####################

## DESIGNATING FIRST RUN AS A VARIABLE
first_run=true # true

## LOOPING THROUGH TRAINING ARRAY
for training_size in ${training_size_increments[@]}; do
    ## DEFINING SAMPLING INPUTS
    sampling_inputs="${training_size},${test_size}"
    ## DEFINING DIRECTORY
    dir_training_size_name="${training_size}_${test_size}"
    dir_training_size_path="${main_dir}/${dir_training_size_name}"
    
    ## DEFINING PATH TO OUTPUT BASH
    path_output_bash_run="${path_output}/${dir_training_size_name}/${output_bash_script}"

    ## DEFINING OUTPUT PATH
    path_output_submit_script="${path_output}/${dir_training_size_name}/${output_submit}"
    
    ## TURNING FIRST RUN OFF
    if [ "${first_run}" == true ]; then
        add_job_list="true"
    else
        add_job_list="false"
    fi
    
    ## LOOPING THROUGH SAMPLING PERC ARRAY
    for sampling_perc in ${sampling_perc_array[@]}; do
        ## DEFINING REPRESENTATION
        representation_inputs="${initial_frame},${last_frame},${split_chunks},${sampling_perc}"
        # "${split_chunks},${sampling_perc},${initial_frame},${last_frame}" # Number of splits 19

        ## RUNNING BASH SCRIPT
        bash "${path_input_bash}" \
                "${dir_training_size_path}" \
                "${cnn_type}" \
                "${representation_types}" \
                "${representation_inputs}" \
                "${mass_frac}" \
                "${cosolvents}" \
                "${solutes}" \
                "${epochs}" \
                "${database_type}" \
                "${sampling_type}" \
                "${sampling_inputs}" \
                "${add_job_list}" \
                "${want_descriptor}" \
                "${num_cross_valid_folds}"

        ## DEFINING OUTPUT NAME
        output_name="$(CNN_generate_name ${representation_types}\
                                         ${representation_inputs}\
                                         ${mass_frac} ${cosolvents} ${solutes} ${epochs} ${database_type} \
                                         ${cnn_type} \
                                         ${sampling_type} \
                                         ${sampling_inputs} \
                                         "${want_descriptor}" \
            )"

        ## GETTING THE OUTPUT NAME AS A BASH SCRIPT
        if [ "${first_run}" == false ]; then
            echo "####" >> "${path_output_bash_run}"
            echo "echo \"Check output at: ${path_output}/${dir_training_size_name}/${output_name}/${output_sim_info}\"" >> "${path_output_bash_run}"
            echo "cd \"${path_output}/${dir_training_size_name}/${output_name}\"; bash ${output_bash_extraction_script} > ${output_sim_info} &" >> "${path_output_bash_run}"
            echo "" >> "${path_output_bash_run}"
            #  2>&1
        fi
    done

    if [ "${first_run}" == false ]; then
        ## WAITING FOR ALL FILES
        echo "" >> "${path_output_bash_run}"
        echo "wait" >> "${path_output_bash_run}"

        ## COPYING SUBMISSION SCRIPT
        cp "${path_submit_script}" "${path_output_submit_script}"

        ## EDITING SUBMISSION SCRIPT
        sed -i "s/_JOB_NAME_/${main_dir}_${dir_training_size_name}/g" "${path_output_submit_script}"
        sed -i "s/_BASHSCRIPT_/${output_bash_script}/g" "${path_output_submit_script}"

        ## ADDING TO JOB LIST
        echo "${path_output_submit_script}" >> "${CNN_JOB_LIST}"
    else
        first_run=false
    fi

done









