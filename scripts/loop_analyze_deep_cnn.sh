#!/bin/bash
# loop_analyze_deep_cnn.sh
# This script loops through the analyze deep cnn code an runs each line by line
# 
# USAGE:
#	bash loop_analyze_deep_cnn.sh
#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bin/global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## LOADING PY36
load_python36
# cnn_project_load_python

## DEFINING JOB LIST
job_list="${ANALYZE_JOB_LIST}"
job_complete_list="${job_list%.txt}_complete.txt"

## LOOPING THROUGH EACH JOB
for each_script in $(cat "${job_list}"); do
	## GETTING DIRNAME
	currentdirname="$(dirname ${each_script})"
	script_name="$(basename ${each_script})"
	## GOING TO DIR
	cd "${currentdirname}"
	## RUN THE COMMAND
	bash "${script_name}"

	## PRINTING TO COMPLETION
	echo "${each_script}" >> "${job_complete_list}"
	## DELETING FIRST LINE
	# sed -i "1d" "${job_list}"

done


