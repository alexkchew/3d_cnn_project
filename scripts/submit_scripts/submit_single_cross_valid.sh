#!/bin/bash 
# submit_single_cross_valid.sh
# This submission script is designed to submit single cross validation
## VARIABLES:
#   _JOB_NAME_ <-- job name
#   _BASHSCRIPT_ <-- bash script

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 168:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## LOADING ENVIRONMENT
source "${HOME}/envs/cs760/bin/activate"

## ADDING PATH VARIABLES
export PYTHONPATH="/home/akchew/.local/lib/python3.4/site-packages:${PYTHONPATH}"
export PYTHONPATH="/usr/lib64/python3.4/site-packages:${PYTHONPATH}"

###SERVER_SPECIFIC_COMMANDS_END

## RUN COMMANDS
