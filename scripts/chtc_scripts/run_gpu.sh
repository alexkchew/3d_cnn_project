#!/bin/bash
# run.sh
# Variables:
#	$1: run process number

## DEFINING VARIABLE
process_num="$1"

## RUNNING COMMAND
bash "extract_deep_cnn_separated_instances.sh" "${process_num}"
