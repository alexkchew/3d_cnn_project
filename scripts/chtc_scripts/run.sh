#!/bin/bash
# run.sh
# Variables:
#	$1: run process number

## LOADING RC
source "chtc_rc.sh"

## DEFINING VARIABLE
process_num="$1"

## RUNNING COMMAND
bash "extract_deep_cnn_separated_instances.sh" "${process_num}"
