#!/bin/bash
# test_gpu.sh
# Variables:
#	$1: run process number

## DEFINING VARIABLE
process_num="$1"

## PRINTING VERSION
python --version
## PRINTING CURRENT WORKING DIRECTORY
pwd

## RUNNING PYTHON SCRIPT
python "test_gpu.py"
