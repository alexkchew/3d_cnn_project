#!/bin/bash
# chtc_rc.sh

###############################
### EXPORTING THE TAR FILES ###
###############################

## UNTAR PYTHON INSTALLATION
if [ ! -e "python" ]; then
	tar -xzf python36.tar.gz
fi

## UNTARRING PACKAGES
if [ ! -e "packages" ]; then
	tar -xzf packages.tar.gz
fi

## DEFINING FUNCTION FOR PYTHON
function python() {
    "$PWD/python/bin/python3" "$@"
}

## ADDING PATHS TO PYTHON
export PATH=$PWD/python/bin:$PATH
export PYTHONPATH=$PWD/packages

## EXPORTING FUNCTION
export -f python
