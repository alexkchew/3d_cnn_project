#!/usr/bin/env python

import tensorflow as tf

print("Cuda?")
tf.test.is_built_with_cuda()

## PRINTING
print("GPUs?")
tf.test.is_gpu_available()
print("List GPUs?")
tf.config.experimental.list_physical_devices('GPU')

## PRINTING ALL DEVICES
tf.config.experimental.list_physical_devices(device_type=None)

