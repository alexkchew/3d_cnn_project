#!/bin/bash
# global_funcs.sh
# This script contains all global functions within this project. 
# AVAILABLE FUNCTIONS:
#   convert_all_commas_to_underscore: converts string comma to underscores
#   CNN_generate_name: CNN nomenclature generator
#   check_model_completion: checks if a model is complete

### LOADING GLOBAL FUNCTIONS
source "${HOME}/bin/bashfiles/server_general_research_functions.sh"

## SOURCING THE GLOBAL VARIABLES (assumed to be within same folder)
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING GLOBAL VARS NAME
global_vars_file_name="global_vars.sh"

## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_vars_file_name}"

## ADDING PYTHON PATH
## CS760 PROJECT
export PYTHONPATH="${CNN_PYTHON_CODES}:$PYTHONPATH"

### FUNCTION TO CONVERT ALL COMMAS TO UNDERSCORE
# The purpose of this function is to convert all commas to underscores, e.g.:
#   10,25,30 -> 10_25_30
function convert_all_commas_to_underscore () {
    ## INPUT: STRING
    input_string_="$1"
    
    ## PRINTING 
    echo "${input_string_}" |  sed -e 's/,/_/g'
}

### FUNCTION TO GENERATE NAME
# The purpose of this function is to generate a name for the CNN
# INPUTS:
#   representation_types: type of representation for your data, e.g. split_avg_nonorm
#   representation_inputs: inputs for representation type, e.g. 5
#   mass_frac: list of mass fractions, e.g. 10,25,50,75
#   cosolvents: list of cosolvents, e.g. DIO,GVL,THF
#   solutes: list of solutes, e.g. CEL,ETBE,FRU,LGA,PDO,XYL,tBuOH 
#   cnn_types: cnn type, e.g. solvent_net
#   epochs: number of epochs, e.g. 500
#   database_type: database type (e.g. 20_20_20)
#   sampling_type: sampling type name, e.g. 'stratified_learning'
#   sampling_inputs: inputs for sampling type    
# OUTPUTS:
#   output_name: name for output directory, e.g.
#       32_32-split_avg_nonorm_planar-5-CEL_ETBE_FRU_LGA_PDO_XYL_tBuOH-DIO-10-vgg16_1
function CNN_generate_name () {
    ## DEFINING INPUT VARIABLES
    representation_types="$1"
    representation_inputs="$2"
    
    ## DETAILS OF TRAINING
    mass_frac="${3}"
    cosolvents="${4}" # DIO,GVL,THF
    solutes="${5}"
    
    ## DEFINING NUMBER OF EPOCHS
    epochs="${6}"

    ## DEFINING DATABASE TYPE
    database_type="${7}"
    
    ## DEFINING CNN TYPES
    cnn_type="${8}"
    
    ## DEFINING SAMPLING DICT
    sampling_type="${9}"

    ## DEFINING INPUTS FOR SAMPLING TYPE
    sampling_inputs="${10}"
    
    ## DEFINING DESCRIPTORS
    want_descriptors="${11:-false}"

    ## CONVERTING MULTIPLE VARIABLES FROM STRING TO COMMA
    representation_inputs=$(convert_all_commas_to_underscore "${representation_inputs}")
    mass_frac=$(convert_all_commas_to_underscore "${mass_frac}")
    cosolvents=$(convert_all_commas_to_underscore "${cosolvents}")
    solutes=$(convert_all_commas_to_underscore "${solutes}")
    sampling_inputs_list=$(convert_all_commas_to_underscore "${sampling_inputs}")
    
    ## DEFINING NEW NAME
    output_name="${database_type}-${representation_types}-${representation_inputs}-${sampling_type}-${sampling_inputs_list}-${cnn_type}-${epochs}-${solutes}-${mass_frac}-${cosolvents}"
    
    if [[ "${want_descriptors}" == true ]]; then
        ## ADDING TO OUTPUT NAME
        output_name="${output_name}-MD"
    fi
    
    ## PRINTING
    echo "${output_name}"
}


### CHECKING IF MODELS ARE COMPLETE
# The purpose of this function is to check if the model is complete after 
# training. If not, this function will return "False" indicating that it 
# did not complete.
# INPUTS:
#   $1: variable to store the output in
#   $2: model prefix to look for, e.g. "model_fold_"
#   $3: number of cross validation folds, e.g. "5"
# OUTPUTS:
#   true/false for whether the model is complete or not. 
# USAGE:
#   check_model_completion job_status model_fold_ 5
function check_model_completion () {
    ## DEFINING INPUTS
    local __resultvar=$1
    model_prefix_="${2-model_fold_}"
    num_cross_fold_="${3-5}"

    ## SEEING IF NUM CROSS VALIDATION FOLD IS NOT 0
    if [[ "${num_cross_fold_}" != 0 ]]; then
        ## SUBTRACTING ONE
        num_cross_fold_="$((${num_cross_fold_}-1))"
    fi
    ## ASSUME JOB IS COMPLETE
    local job_is_complete=true

    ## LOOPING AND CHECKING 
    for idx in $(seq 0 ${num_cross_fold_}); do
        if [[ "${num_cross_fold_}" != 0 ]]; then
            ## DEFINING MODEL NAME
            file_name_="${model_prefix_}${idx}.pickle"
        else
            ## DEFINING MODEL NAME
            file_name_="${model_prefix_}.pickle"
        fi

        ## CHECKING IF FILE EXISTS
        if [[ -f "${file_name_}" ]]; then
            echo "${file_name_} exists"
        else
            echo "${file_name_} does not exist"
            job_is_complete=false
        fi

    done

    eval $__resultvar="${job_is_complete}"
}



