#!/bin/bash
# global_vars.sh
# This contains all global variables for this project.
# Note that all global variables are in all-caps

################################
### MAIN DIRECTORY VARIABLES ###
################################
## DEFINING SCRIPT DIRECTORY
CNN_BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CNN_SCRIPT_DIR="$( cd "${CNN_BIN_DIR}/../" >/dev/null 2>&1 && pwd )"

## DEFINING IMPORTANT DIRECTORY
CNN_MAIN_DIR=$(dirname "${CNN_SCRIPT_DIR}")

## DEFINING EXTRACTION DIR
CNN_EXTRACT_DIR="${CNN_SCRIPT_DIR}/extract_scripts"

## DEFINING ENVIRONMENT
CNN_ENV="${HOME}/envs/cs760/bin/activate"

## DEFINING DATABASE, ETC.
CNN_DATABASE="${CNN_MAIN_DIR}/database"
CNN_COMBINED_DATA="${CNN_MAIN_DIR}/combined_data_set"
CNN_OUTPUT_DATA="${CNN_MAIN_DIR}/simulations"

## DEFINING MAIN AREA FOR PYTHON CODE
CNN_PYTHON_CODES="${CNN_MAIN_DIR}/python_scripts"

## DEFINING MAIN AREA FOR EXPERIMENTAL DATA
CNN_EXP_DATA="${CNN_DATABASE}/Experimental_Data"

## DEFINING SUBMISSION SCRIPTS
CNN_SUBMIT_SCRIPTS="${CNN_SCRIPT_DIR}/submit_scripts"

## DEFININING JOB LIST
CNN_JOB_LIST="${CNN_SCRIPT_DIR}/job_list.txt"
ANALYZE_JOB_LIST="${CNN_SCRIPT_DIR}/analyze_list.txt"
ANALYZE_ERR_LIST="${CNN_SCRIPT_DIR}/err_analyze_list.txt"